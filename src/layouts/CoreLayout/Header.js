import React, { Component } from "react";
import { Button, Col, Container ,Row } from "reactstrap";


class Header extends Component {
  render() {
    return (
      <div className="header">
        <Col xs="3" sm="1">
          <h1 className="logo"><img alt="logo" src={require("./../../assets/images/logo.png")} /></h1>
        </Col>
        <Col xs="3" sm="11">
          <div className="right_con">
            <span><a href="#"><i className="fa fa-envelope" aria-hidden="true"></i></a></span>
            <span><a href="#"><i className="fa fa-bell" aria-hidden="true"></i></a></span>
            <span className="mainContainer1">
              <a href="#"><i className="fa fa-user-circle" aria-hidden="true"></i>Hi, Joseph</a>
            </span>
          </div>
        </Col>
      </div>
    );
  }
}

export default Header;

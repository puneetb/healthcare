import React, { Component } from "react";
import { Button, Col, Container ,Row } from "reactstrap";
import { browserHistory, Link } from "react-router";

class Sidebar extends Component {
  render() {
    return (
      <Col xs="3" sm="1" className="wpad_0 res_wpad_0">
        <div className="side_nav">
          <ul className="nav_ul">
            <li className="nav_li">
              <Link to={"/patient/"}>
                <i className="fa fa-home" aria-hidden="true"></i>
              </Link>
            </li>
            <li className="nav_li">
              <Link to={"/patient/list"}>
                <i className="fa fa-hospital-o" aria-hidden="true"></i>
              </Link>
            </li>
            <li>
              <Link to={"/patient/family_history"}>
                <i className="fa fa-clock-o" aria-hidden="true"></i>
              </Link>
            </li>
            <li>
              <a href="#"><i className="fa fa-envelope" aria-hidden="true"></i></a>
            </li>
            <li>
              <a href="#"><i className="fa fa-users" aria-hidden="true"></i></a>
            </li>
            <li>
              <a href="#"> <i className="fa fa-flask" aria-hidden="true"></i></a>
            </li>
            <li>
              <a href="#"> <i className="fa fa-envelope" aria-hidden="true"></i></a>
            </li>
            <li>
              <a href="#"> <i className="fa fa-cog" aria-hidden="true"></i></a>
            </li>
          </ul>
        </div>
      </Col>
    );
  }
}

export default Sidebar;

import React, { Component } from "react";
import "./CoreLayout.css";
import { Button, Col, Container, Row } from "reactstrap";
import Header from "./Header";
import Sidebar from "./Sidebar";


import Alert from "react-s-alert";

import "react-s-alert/dist/s-alert-default.css";
class CoreLayout extends Component {


  render() {
    const { children } = this.props;
    const classes = {};

    return (
      <div className="mainContainer">
        <Header />
        <Alert />
        <div className="main_con">
          <Sidebar />
          {React.cloneElement(children, { classes })}
        </div>
      </div>
    );
  }
}

export default CoreLayout;

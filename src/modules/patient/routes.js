import { injectReducer } from "app/reducers";
import Intake from "./components/intake";

const errorLoading = (err) => {
  console.error("Dynamic page loading failed", err);
};
export default (store) => ({
  path: "patient",
  indexRoute: {
    getComponent(nextState, cb) {
      require.ensure([], (require) => {
        const intake = require("./components/intake").default;
        const reducer = require("./reducer").default;
        injectReducer(store, { key: "patient", reducer });
        cb(null, intake);
      /* Webpack named bundle   */
      }, "patient");
    },
  /*  Async getComponent is only invoked when route matches   */
  },
  childRoutes: [{
    path: "profile/*",
    name: "Patient Profile",
    // component: SaleForm,
    getComponent(nextState, cb) {
      require.ensure([], (require) => {
        /*  Webpack - use require callback to define for bundling   */
        const profile = require("./components/profile").default;
        const reducer = require("./reducer").default;

        /*  Add the reducer to the store on key "counter"  */
        injectReducer(store, { key: "patient", reducer });
        /*  Return getComponent   */
        cb(null, profile);

        /* Webpack named bundle   */
      }, "patient");
    },
  },
  {
    path: "list",
    name: "Patient Listing",
    // component: SaleForm,
    getComponent(nextState, cb) {
      require.ensure([], (require) => {
      /*  Webpack - use require callback to define dependencies for bundling   */
        const list = require("./components/list").default;
        const reducer = require("./reducer").default;
        /*  Add the reducer to the store on key "counter"  */
        injectReducer(store, { key: "patient", reducer });
        /*  Return getComponent   */
        cb(null, list);
        /* Webpack named bundle   */
      }, "patient");
    },
  },
  {
    path: "soapNote",
    // component: SaleForm,
    getComponent(nextState, cb) {
      require.ensure([], (require) => {
      /*  Webpack - use require callback to define dependencies for bundling   */
        const soap_note = require("./components/soap_note").default;
        const reducer = require("./reducer").default;
        /*  Add the reducer to the store on key "counter"  */
        injectReducer(store, { key: "patient", reducer });
        /*  Return getComponent   */
        cb(null, soap_note);
        /* Webpack named bundle   */
      }, "patient");
    },
  },
  {
    path: "vitals",
    name: "Patient Vitals",
    // component: SaleForm,
    getComponent(nextState, cb) {
      require.ensure([], (require) => {
        /*  Webpack - use require callback to define dependencies for bundling   */
        const list = require("./components/vitals/index").default;
        const reducer = require("./reducer").default;
        /*  Add the reducer to the store on key "counter"  */
        injectReducer(store, { key: "patient", reducer });
        /*  Return getComponent   */
        cb(null, list);
        /* Webpack named bundle   */
      }, "patient");
    },
  },
  {
    path: "family_history",
    name: "Patient Family History",
    // component: SaleForm,
    getComponent(nextState, cb) {
      require.ensure([], (require) => {
        /*  Webpack - use require callback to define dependencies for bundling   */
        const family_history = require("./components/family_history/index").default;
        const reducer = require("./reducer").default;
        /*  Add the reducer to the store on key "counter"  */
        injectReducer(store, { key: "patient", reducer });
        /*  Return getComponent   */
        cb(null, family_history);
        /* Webpack named bundle   */
      }, "patient");
    },
  },
  {
    path: "social_history",
    name: "Patient Social History",
    // component: SaleForm,
    getComponent(nextState, cb) {
      require.ensure([], (require) => {
        /*  Webpack - use require callback to define dependencies for bundling   */
        const social_history = require("./components/social_history/index").default;
        const reducer = require("./reducer").default;
        /*  Add the reducer to the store on key "counter"  */
        injectReducer(store, { key: "patient", reducer });
        /*  Return getComponent   */
        cb(null, social_history);
        /* Webpack named bundle   */
      }, "patient");
    },
  },
  ],
});

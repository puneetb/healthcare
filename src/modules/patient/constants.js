import { SERVER_URL, createRequestActionTypes } from "app/globalConstants";

export const GET_QUOTE_DATA_API = `${SERVER_URL}/quote`; // GET - /quoteData?symbol=AAPL&field=4+10+11
export const VALIDATE_EMAILS_EXISTS = `${SERVER_URL}/users/getUser`; // GET - /quoteData?symbol=AAPL&field=4+10+11
export const CHECK_PATIENT_EXIST = `${SERVER_URL}/HC_Patient/PatientExist`;
export const CREATE_PATIENT_API = `${SERVER_URL}/HC_Patient/patient`;
export const UPDATE_PATIENT_ADDRESS = `${SERVER_URL}/HC_Patient/patient-address`;
export const UPDATE_PATIENT_INSURANCE_DETAILS = `${SERVER_URL}/HC_Patient/patient-insurance-detail`;
export const FETCH_PATIENT = `${SERVER_URL}/HC_Patient/patient`;
export const SAVE_PHONE_NUMBER = `${SERVER_URL}/HC_Patient/patient-phone-number/postMultiple`;
export const FETCH_PATIENT_ADDRESS = `${SERVER_URL}/HC_Patient/patient-address`;
export const FETCH_PATIENT_INSURANCE = `${SERVER_URL}/HC_Patient/patient-insurance-detail`;
export const FETCH_PATIENT_ALL_INFO = `${SERVER_URL}/HC_Patient/patient`;
export const SAVE_PREFRENCES = `${SERVER_URL}/HC_Patient/patient-preference`;
export const FETCH_ALL_PATIENTS = `${SERVER_URL}/HC_Patient/patient`;
export const UPDATE_PREFERENCES = `${SERVER_URL}/HC_Patient/PatientPreference`;
// get listing of patient vitals
export const FETCH_PATIENT_VITALS = `${SERVER_URL}/HC_Patient/patient-vitals`;
// get state data by country
export const GET_STATE_BY_COUNTRY = `${SERVER_URL}/HC_Patient/api/MasterData/GetStateByCountryID`;
// patient family history
export const FAMILY_HISTORY = `${SERVER_URL}/HC_Patient/patient-medical-family-history`;
export const UPDATE_FAMILY_HISTORY = `${SERVER_URL}/HC_Patient/patient-medical-family-history/updatemedicalfamilyhistory`;

// patient social history
export const SOCIAL_HISTORY = `${SERVER_URL}/HC_Patient/patient-social-history`;

export const patientActionTypes = {
  master_list: createRequestActionTypes("MASTERDATA"),
  patient_exist: createRequestActionTypes("PATIENT_EXIST"),
  create_patient: createRequestActionTypes("CREATE_PATIENT"),
  update_mailing_address: createRequestActionTypes("UPDATE_ADDRESS"),
  save_address: createRequestActionTypes("SAVE_ADDRESS"),
  save_insurance: createRequestActionTypes("SAVE_INSURANCE"),
  fetch_patient: createRequestActionTypes("FETCH_PATIENT"),
  save_phone_number: createRequestActionTypes("SAVE_PHONE_NUMBER"),
  fetch_address: createRequestActionTypes("FETCH_ADDRESS"),
  fetch_patient_insurance: createRequestActionTypes("FETCH_PATIENT_INSURANCE"),
  fetch_patient_all_info: createRequestActionTypes("FETCH_PATIENT_ALL_INFO"),
  save_preferences: createRequestActionTypes("SAVE_PREFRENCE"),
  fetch_all_patients: createRequestActionTypes("FETCH_ALL_PATIENTS"),
  clear_form: createRequestActionTypes("CLEAR_FORM"),
  patient_vitals: createRequestActionTypes("FETCH_PATIENT_VITALS"),
  get_state_by_country: createRequestActionTypes("GET_STATE_BY_COUNTRY"),
  save_family_history: createRequestActionTypes("SAVE_FAMILY_HISTORY"),
  fetch_patient_relationships: createRequestActionTypes("FETCH_PATIENT_RELATIONSHIPS"),
  fetch_history_detail: createRequestActionTypes("FETCH_HISTORY_DETAIL"),
  save_social_history: createRequestActionTypes("SAVE_SOCIAL_HISTORY"),
  fetch_social_history: createRequestActionTypes("FETCH_SOCIAL_HISTORY"),
  get_mailing_state_by_country: createRequestActionTypes("GET_MAILING_STATE_BY_COUNTRY"),
};

export const PATIENT_TITLE = [
  { id: "mr", value: "Mr." },
  { id: "ms", value: "Ms." },
];

export const COMM_PREFRENCES = [
  { id: "1", value: "Do not contact me at Home" },
  { id: "2", value: "Leave message with department/office name and call-back number on answering maching" },
];

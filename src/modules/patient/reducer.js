import { patientActionTypes } from './constants'

const initialState = {
  list: [],
  item: null,
  error: null,
  loading: false,
  ispatientAvailable: null,
  patient_created: false,
  insurance_updated: false,
  address_updated: false,
  all_patients: [],
  state_list: [],
  mailing_state: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
  case patientActionTypes.master_list.REQUEST:
  case patientActionTypes.patient_exist.REQUEST:
  case patientActionTypes.create_patient.REQUEST:
  case patientActionTypes.save_address.REQUEST:
  case patientActionTypes.save_insurance.REQUEST:
  case patientActionTypes.fetch_patient.REQUEST:
  case patientActionTypes.save_phone_number.REQUEST:
  case patientActionTypes.fetch_address.REQUEST:
  case patientActionTypes.fetch_patient_insurance.REQUEST:
  case patientActionTypes.fetch_patient_all_info.REQUEST:
  case patientActionTypes.fetch_all_patients.REQUEST:
  case patientActionTypes.get_state_by_country.REQUEST:
  case patientActionTypes.patient_vitals.REQUEST:
  case patientActionTypes.save_family_history.REQUEST:
  case patientActionTypes.fetch_patient_relationships.REQUEST:
  case patientActionTypes.fetch_history_detail.REQUEST:
  case patientActionTypes.save_social_history.REQUEST:
  case patientActionTypes.fetch_social_history.REQUEST:
    return {
      ...state,
      loading: true,
      address_updated: false,
    };
  case patientActionTypes.fetch_patient_all_info.REQUEST:
  // case patientActionTypes.fetch_address.REQUEST:
  case patientActionTypes.fetch_patient_insurance.REQUEST:
  case patientActionTypes.clear_form.REQUEST:
    return {
      ...state,
      patient_data: payload,
      address_data: payload,
      insurance_data: payload,
      family_history: payload,
    };
  case patientActionTypes.master_list.SUCCESS:
    return {
      ...state,
      master_data: payload,
      loading: false,
      patient_created: false,
    };
  case patientActionTypes.patient_exist.SUCCESS:
    console.log(...state);
    return {
      ...state,
      ispatientAvailable: payload,
      loading: false,
    };
  case patientActionTypes.create_patient.SUCCESS:
    return {
      ...state,
      patient_created: true,
      loading: false,
      // patient_data: payload
    };

  case patientActionTypes.create_patient.FAILURE:
    return {
      ...state,
      loading: false,
    };
  case patientActionTypes.update_mailing_address.SUCCESS:
    return {
      ...state,
      address_data: payload,
      loading: false,
    };
  case patientActionTypes.save_address.SUCCESS:
    return {
      ...state,
      address_updated: true,
      address_data: payload,
    };
  case patientActionTypes.save_insurance.SUCCESS:
    return {
      ...state,
      insurance_updated: true,
      insurance_data: payload,
      loading: false,
    };
  case patientActionTypes.fetch_patient.SUCCESS:
    return {
      ...state,
      patient_data: payload,
      loading: false,
      patient_created: false,
    };
  case patientActionTypes.save_phone_number.SUCCESS:
    return {
      ...state,
      loading: false,
    };
  case patientActionTypes.fetch_address.SUCCESS:
    return {
      ...state,
      address_updated: false,
      address_data: payload,

      loading: false,
    };
  case patientActionTypes.fetch_patient_insurance.SUCCESS:
    return {
      ...state,
      insurance_data: payload,
      loading: false,
    };
  case patientActionTypes.fetch_patient_all_info.SUCCESS:
    return {
      ...state,
      patient_global_data: payload,
      loading: false,
    };
  case patientActionTypes.save_preferences.SUCCESS:
    return {
      ...state,
      loading: false,
    };
  case patientActionTypes.fetch_all_patients.SUCCESS:
    return {
      ...state,
      loading: false,
      all_patients: payload,
    };
  case patientActionTypes.patient_vitals.SUCCESS:
    return {
      ...state,
      loading: false,
      patient_vitals: payload,
    };
  case patientActionTypes.get_state_by_country.SUCCESS:
    return {
      ...state,
      loading: false,
      state_list: payload,
    };
  case patientActionTypes.save_family_history.SUCCESS:
  case patientActionTypes.save_social_history.SUCCESS:
    return {
      ...state,
      loading: false,
    };
  case patientActionTypes.fetch_patient_relationships.SUCCESS:
    return {
      ...state,
      loading: false,
      historyList: payload,
    };
  case patientActionTypes.fetch_history_detail.SUCCESS:
    return {
      ...state,
      loading: false,
      family_history: payload.data.attributes,
    };
  case patientActionTypes.fetch_social_history.SUCCESS:
    return {
      ...state,
      loading: false,
      social_history: payload.data[0].attributes,
    };
  case patientActionTypes.get_mailing_state_by_country.SUCCESS:
    return {
      ...state,
      loading: false,
      mailing_state: payload,
    };
  default:
    return state;
  }
};

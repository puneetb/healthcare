import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Field, reduxForm, reset } from "redux-form";
import { Button, Card, CardText, CardTitle, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Form, FormGroup, Input, Label, Nav, NavItem, NavLink, Row, TabContent, TabPane } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { maskInputField, RadioButton, renderDateField, renderField, renderFieldTmp, renderSelectField, renderUploadField } from "app/reduxForm/components/";
import { email, maxLength15, number, onlyLetters, required, requiredDropdown, tooOld } from "app/reduxForm/validations";
import { checkPatientExist, fetchMasterTablesData, getPatientData, insertUpdatePatientData, UpdatePatientData } from "./../actions";
import { COMM_PREFRENCES, PATIENT_TITLE } from "./../constants";
import { calculateAge, handleFormErrors } from "utils";
import Alert from "react-s-alert";
import { browserHistory, Link } from "react-router";

import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";

const validate = (values) => {
  const errors = {};

  if (!values.dob) {
    // errors.dob = 'Please enter DOB.'
  }

  if (values.Title == "0") {
    errors.Title = "Please enter DOB.";
  }

  if (!values.SSN) {
    errors.SSN = "Please enter SSN.";
  } else {

    if (values.SSN && values.SSN != "" && values.SSN.match(/\d/g).length < 9) {
      errors.SSN = "Please enter valid SSN.";
    }
  }
  // console.log(errors)
  return errors;
};

// console.log("locastorage: ", localStorage.getItem('patientID_fk'), "initial state: ", initialState)

class Demographic extends Component {


    static propTypes = {
      fetchMasterTablesData: PropTypes.func.isRequired,
      checkPatientExist: PropTypes.func.isRequired,
      insertUpdatePatientData: PropTypes.func.isRequired,
    }

    constructor(props) {
      super(props);

      this.state = {
        activeTab: "1",
        startDate: "",
        referraldate: "",
        patient_age: false,
        gender: "1",
        programStartDate: "",
        master_data: { masterRace: [], masterEthnicity: [], masterPreferredLanguage: [], masterSuffix: [], masterMaritalStatus: [], masterPatientStatus: [], masterRelationship: [], provider: [], masterProgram: [], masterService: [], masterReferral: [] },
        ispatientAvailable: null,
        loading: false, error: false,
        homePhoneComm: "1",
        patient_created: false,
        patientID_fk: localStorage.getItem("patientID_fk"),
        patient_data: {},
        workPhoneComm: "1",
        addClientCaseLoad: false,
        preference_id: null,
        db: null,
        programDate: null,
      };
      this.handleChange = this.handleChange.bind(this);
      this.handleProgramChange = this.handleProgramChange.bind(this);
      this.handleReferralChange = this.handleReferralChange.bind(this);
      // console.log("locastorage: ")

    }

    componentWillMount() {

      this.props.reset("Demographic");
      this.props.fetchMasterTablesData();
      if (this.state.patientID_fk != undefined
            && Object.keys(this.state.patient_data).length == 0
      ) {
        this.props.getPatientData(this.state.patientID_fk);
      }
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.master_data != undefined) {
        this.setState({
          master_data: nextProps.master_data,
          ispatientAvailable: nextProps.ispatientAvailable,
          loading: nextProps.loading,
        });
      }

      if (nextProps.patient_created != this.state.patient_created) {
        this.setState({
          patient_created: nextProps.patient_created,
        });
      }

      if (nextProps.patient_created && this.props.patient_created !== nextProps.patient_created && nextProps.patient_created == true) {
        this.props.onSubmitSuccess("2");
      }

      if (nextProps.ispatientAvailable === 2) {
        Alert.error("Patient already exists.", {
          html: true,
        });
      }

      if (nextProps.patient_data && this.props.patient_data !== nextProps.patient_data) {
        this.setState({
          imagePreviewUrl: nextProps.patient_data.hasOwnProperty("PhotoThumbnailPath") != null ? nextProps.patient_data.PhotoThumbnailPath : null,
          startDate: moment(nextProps.patient_data.dob),
          dob: moment(nextProps.patient_data.dob),
        });
      }
      if (nextProps.patient_data && this.props.patient_data !== nextProps.patient_data) {
        this.setState({
          patient_data: nextProps.patient_data,
          // imagePreviewUrl: nextProps.patient_data.hasOwnProperty("photoThumbnailPath") != null ? nextProps.patient_data.photoThumbnailPath : null,
          startDate: moment(nextProps.patient_data.DOB),
          dob: moment(nextProps.patient_data.DOB),
          homePhoneComm: nextProps.patient_data.hasOwnProperty("PatientHomeCommPreferences_fk") != null ? nextProps.patient_data.PatientHomeCommPreferences_fk : null,
          workPhoneComm: nextProps.patient_data.hasOwnProperty("PatientOfficeCommPreferences_fk") != null ? nextProps.patient_data.PatientOfficeCommPreferences_fk : null,
          preference_id: nextProps.patient_data.hasOwnProperty("preference_id") != null ? nextProps.patient_data.preference_id : null,
          programStartDate: nextProps.patient_data.hasOwnProperty("ProgramStartDate") != null ? moment(nextProps.patient_data.ProgramStartDate) : null,
          referraldate: nextProps.patient_data.hasOwnProperty("ReferralDate") != null ? moment(nextProps.patient_data.ReferralDate) : null,
          addClientCaseLoad: nextProps.patient_data.hasOwnProperty("AddClientToCaseLoad") != null ? nextProps.patient_data.AddClientToCaseLoad : false,
          gender: nextProps.patient_data.hasOwnProperty("Gender_fk") != null ? nextProps.patient_data.Gender_fk : null,
          programDate: nextProps.patient_data.hasOwnProperty("ProgramStartDate") != null ? moment(nextProps.patient_data.ProgramStartDate).format("YYYY-MM-DD") : null,
          referralDateFormatted: nextProps.patient_data.hasOwnProperty("ReferralDate") != null ? moment(nextProps.patient_data.ReferralDate).format("YYYY-MM-DD") : null,
          // referraldate:
          // programStartDate
        });
      }
    }


    handleChange(date) {
      const patient_age = calculateAge(date);

      this.setState({
        startDate: date,
        patient_age: patient_age,
        dob: moment(date).format("YYYY-MM-DD"),
      });
    }

    handleReferralChange(date) {
      this.setState({
        referraldate: date,
        referralDateFormatted: moment(date).format("YYYY-MM-DD"),
      });
    }

    handleProgramChange(date) {
      this.setState({
        programStartDate: date,
        programDate: moment(date).format("YYYY-MM-DD"),
      });
    }

    onSubmitubmitHandler(formProps) {
      formProps.photoPath = "";
      const patient_updated_data = {
        dob: this.state.dob,
        PhotoBase64: this.state.imagePreviewUrl != null && this.state.imagePreviewUrl.indexOf("data:image") > -1 ? this.state.imagePreviewUrl : null,
        gender_fk: this.state.gender,
        createdBy_fk: 1,
        updatedBy_fk: 1,
        firstName: formProps.FirstName,
        lastName: formProps.LastName,
        middleName: formProps.MiddleName,
        mrn: formProps.MRN,
        title: formProps.Title,
        suffix_fk: formProps.Suffix_fk,
        ssn: formProps.SSN,
        email: formProps.Email,
        maritalStatus_fk: formProps.MaritalStatus_fk,
        race_fk: formProps.Race_fk,
        secondaryRace_fk: formProps.SecondaryRace_fk,
        ethnicity_fk: formProps.Ethnicity_fk,
        clientStatus_fk: formProps.ClientStatus_fk,
        primaryProvider_fk: formProps.PrimaryProvider_fk,
        emergencyContactFirstName: formProps.EmergencyContactFirstName,
        emergencyContactLastName: formProps.EmergencyContactLastName,
        emergencyContactPhone: formProps.EmergencyContactPhone,
        emergencyContactRelationship_fk: formProps.EmergencyContactRelationship_fk,
        citizenship_fk: formProps.citizenship_fk,
        id: this.state.patientID_fk,
      };
      // console.log("patient_updated_data==", patient_updated_data);
      this.props.checkPatientExist(patient_updated_data);

      if (!formProps.hasOwnProperty("button_name")) {
        let prfferObj = this.makePreferredLanguageObject(formProps);
        let dataObj = {};
        // console.log("==----79", prfferObj)
        dataObj.patient_data = patient_updated_data;
        dataObj.preferenceData = prfferObj;

        // console.log("====", dataObj)
        this.props.insertUpdatePatientData(dataObj);
      }
    }

    makePreferredLanguageObject(formData) {
      let preferredObj = {
        id: this.state.preference_id,
        preferredLanguage_fk: formData.PreferredLanguage_fk,
        patientID_fk: this.state.patientID_fk,
        program_fk: formData.Program_fk,
        programStartDate: this.state.programDate,
        serviceRequested_fk: formData.ServiceRequested_fk,
        referral_fk: formData.Referral_fk,
        referralReason: formData.ReferralReason,
        referralDate: this.state.referralDateFormatted,
        patientHomeCommPreferences_fk: this.state.homePhoneComm,
        patientOfficeCommPreferences_fk: this.state.workPhoneComm,
        addClientToCaseLoad: this.state.addClientCaseLoad,
        CreatedBy_fk: 1,
        isVerified: true,
      };


      return preferredObj;
    }

    renderExistResult() {
      if (this.state.ispatientAvailable == 2) {
        return (
          <div className={this.props.classes.redColor}>Patient already exist with above information.</div>
        );
      } else {
        return (
          <div className={this.props.classes.successCls}>Patient available to register</div>
        );
      }

    }

    _handleImageChange(e) {
      e.preventDefault();

      let reader = new FileReader();
      let file = e.target.files[0];

      reader.onloadend = () => {
        this.setState({
          file: file,
          imagePreviewUrl: reader.result,
        });

      };

      reader.readAsDataURL(file);
    }


    render() {
      // console.log("new sate",this.state)
      const { handleSubmit, pristine, reset, submitting, onSubmitSuccess, match, location, history } = this.props;
      const { classes } = this.props;
      const { masterRace, masterEthnicity, masterPreferredLanguage, masterPatientStatus, masterSuffix, masterRelationship, provider, masterProgram, masterService, masterMaritalStatus, masterReferral } = this.state.master_data;
      const dropdownData = [{
        "id": "18",
        "value": "Mr",
      }, {
        "id": "17",
        "value": "Ms",
      }];

      return (
        <div>
          <form onSubmit={handleSubmit(this.onSubmitubmitHandler.bind(this))}>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <Col sm="2">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="Title" emptyValue="Select Title"
                          templateList={PATIENT_TITLE}
                          component={renderSelectField}
                          label="Title" plaeholder="Title"
                          validate={[required("Title")]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="2"></Col>
                  <Col sm="4">
                    <div className="txtbx3">
                      <div className="col-sm">
                        <Field name="FirstName" type="text" component={renderField}
                          label="First Name" plaeholder="First Name"
                          validate={[required("First Name"), onlyLetters]}
                          // onChangeEvent={false}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="MiddleName" type="text" component={renderField}
                          label="Middle Name" plaeholder="Middle Name"
                          validate={[onlyLetters]}
                          // value={this.state.symbol}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <Col sm="4">
                    <div className="txtbx3">
                      <div className="col-sm">
                        <Field name="LastName" type="text" component={renderFieldTmp}
                          label="Last Name" plaeholder="Last Name"
                          validate={[required("Last Name"), onlyLetters]}
                          // value={this.state.symbol}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="2">
                    <div className="txtbx3">
                      <div className="col-sm">
                        <Field name="Suffix_fk" emptyValue="Select Suffix"
                          templateList={masterSuffix}
                          component={renderSelectField}
                          label="Suffix"
                          // validate={[ required('Suffix')  ]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="2">
                    <div className="txtbx3">
                      <div className="col-sm">
                        <Field name="ClientStatus_fk" emptyValue="Select Status"
                          templateList={masterPatientStatus}
                          component={renderSelectField}
                          label="Client Status"
                          validate={[required("Client Status")]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="3">
                    <div className="txtbx3">
                      <div className="col-sm">
                        <Field name="dob" type="text"
                          component={renderDateField}
                          label="DOB" placeholderText="dd/mm/yyyy"
                          selected={this.state.startDate}
                          onDateChange={this.handleChange}
                          showYearFlag={true}
                          showMonthFlag={true}
                          classes={classes}
                          // validate={[ required('DOB')  ]}
                          maxDateVal={moment()}
                        />
                      </div>
                    </div>
                  </Col>

                  <Col sm="1">
                    <div className="txtbx3">
                      <div className="col-sm">
                        <FormGroup>
                          <label>AGE</label>
                          <span className="yr_dv">{this.state.patient_age}</span>
                        </FormGroup>
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        {/* <Field name="ssn" type="text" component={renderField}
                                            label="SSN" plaeholder="122-22-12345" 
                                            validate={[ required('SSN')  ]}
                                            classes={classes}
                                        />*/}
                        <Field name="SSN"
                          component={maskInputField}
                          placeholder="122-22-12345"
                          maskFormat="999-99-9999"
                          label="SSN"
                          // validate={[ required('SSN') ]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="PreferredLanguage_fk" emptyValue="Select Language"
                          templateList={masterPreferredLanguage}
                          component={renderSelectField}
                          label="Preferred Language"
                          validate={[required("Language")]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="Email" type="text" component={renderField}
                          label="Email" plaeholder="e.g user@example.com"
                          validate={[required("Email"), email]}
                          // value={this.state.symbol}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <label>Gender</label>
                        <div className="radio_dv"><Field
                          name="status"
                          component="input"
                          type="radio"
                          id="radio-1"
                          className="radio-custom"
                          value="1"
                          checked={ this.state.gender == "1" ? true : false }
                          onClick={ () => this.setState({ gender: "1" }) } />
                        <label htmlFor="radio-1" className="radio-custom-label">Male</label>
                        <Field
                          name="status"
                          component="input"
                          type="radio"
                          id="radio-2"
                          className="radio-custom"
                          value="2"
                          checked={ this.state.gender == "2" ? true : false }
                          onClick={ () => this.setState({ gender: "2" }) } />
                        <label htmlFor="radio-2" className="radio-custom-label">Female</label>
                        <Field
                          name="status"
                          component="input"
                          type="radio"
                          id="radio-3"
                          className="radio-custom"
                          value="3"
                          checked={ this.state.gender == "3" ? true : false }
                          onClick={ () => this.setState({ gender: "3" }) } />
                        <label htmlFor="radio-3" className="radio-custom-label">Other</label></div>
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="PrimaryProvider_fk" emptyValue="Select Provider"
                          templateList={provider}
                          component={renderSelectField}
                          label="Primary Provider"
                          // validate={[ required('Provider')  ]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="MaritalStatus_fk" emptyValue="Marital Status"
                          templateList={masterMaritalStatus}
                          component={renderSelectField}
                          label="Marital Status"
                          // validate={[ required('Marital Status')  ]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="Race_fk" emptyValue="Select Race"
                          templateList={masterRace}
                          component={renderSelectField}
                          label="Race"
                          validate={[required("Race")]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="SecondaryRace_fk" emptyValue="Select Secondary Race"
                          templateList={masterRace}
                          component={renderSelectField}
                          label="Secondary Race"
                          // validate={[ required('Secondary Race')  ]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="Ethnicity_fk" emptyValue="Select Ethnicity"
                          templateList={masterEthnicity}
                          component={renderSelectField}
                          label="Ethnicity"
                          // validate={[ required('Ethnicity')  ]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="MRN" type="text" component={renderField}
                          label="MRN Number" plaeholder="MRN Number"
                          validate={[number]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="profile_photo" type="file" component= {renderUploadField}
                          onChange={(e) => this._handleImageChange(e)}
                          label="Patient Photo"
                        />
                      </div>
                      {this.state.imagePreviewUrl ? (<img src={this.state.imagePreviewUrl} width="100" />) : ""}
                    </div>
                  </Col>
                  {
                    !this.state.patientID_fk ? (
                      <Col sm="4">
                        <div className="check_dv classes.res_check_dv">
                          <h2>Check If Patient Already Exists</h2>
                          <Button type="submit" name="check_existance"
                            color="success" className="check_btn}"
                            onClick={handleSubmit((values) => this.onSubmitubmitHandler({ ...values, button_name: "check_existance" }))}
                          >
                                                CHECK
                          </Button>

                          {this.state.ispatientAvailable ? this.renderExistResult() : ""}
                        </div>
                      </Col>
                    ) : ""
                  }
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <div className="patient_dv">
                    <h2 className="sub_head">Emergency Contact</h2>
                  </div>
                  <Col sm="12">
                    <Col sm="4">
                      <div className="txtbx1">
                        <div className="col-sm">
                          <Field name="EmergencyContactFirstName" type="text" component={renderField}
                            label="First Name" plaeholder="First Name"
                            // validate={[ required('First Name')  ]}
                            classes={classes}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col sm="4">
                      <div className="txtbx1">
                        <div className="col-sm">
                          <Field name="EmergencyContactLastName" type="text" component={renderField}
                            label="Last Name" plaeholder="Last Name"
                            // validate={[ required('Last Name')  ]}
                            classes={classes}
                          />
                        </div>
                      </div>
                    </Col>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <Col sm="12">
                    <Col sm="4">
                      <div className="txtbx1">
                        <div className="col-sm">
                          <Field name="EmergencyContactPhone"
                            component={maskInputField}
                            placeholder="Phone"
                            maskFormat="(999)-999-9999"
                            label="Phone"
                            classes={classes}
                            // validate={[ required('Phone') ]}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col sm="4">
                      <div className="txtbx1">
                        <div className="col-sm">
                          <Field name="EmergencyContactRelationship_fk" emptyValue="Select Relationship"
                            templateList={masterRelationship}
                            component={renderSelectField}
                            label="Relationship"
                            // /validate={[ required('Relationship')  ]}
                            classes={classes}
                          />
                        </div>
                      </div>
                    </Col>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <div className="patient_dv">
                    <h2 className="sub_head">Patient Communication Prefrences</h2>
                  </div>
                </div>
                <Col sm="12">
                  <div className="first_tab">
                    <Col sm="12">
                      <div className="txtbx1">
                        <div className="col-sm">
                          <label>Home Telephone Number</label>
                          <div>
                            <Field
                              name="homePhoneComm"
                              component="input"
                              type="radio"
                              id="homephone1"
                              className="radio-custom"
                              value={1}
                              checked={ this.state.homePhoneComm == "1" ? true : false }
                              onChange={ (e) => this.setState({ homePhoneComm: e.target.value }) }
                            />
                            <label htmlFor="homephone1" className="radio-custom-label">Do not contact me at Home</label>
                          </div>
                          <div>
                            <Field
                              name="homePhoneComm"
                              component="input"
                              type="radio"
                              id="homephone2"
                              className="radio-custom"
                              value={2}
                              checked={ this.state.homePhoneComm == "2" ? true : false }
                              onChange={ (e) => this.setState({ homePhoneComm: e.target.value }) } />
                            <label htmlFor="homephone2" className="radio-custom-label">Leave message with department/office name and call-back number on answering maching</label>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col sm="12">
                      <div className="txtbx1">
                        <div className="col-sm">
                          <label>Work Telephone Number</label>
                          <div>
                            <Field
                              name="workPhoneComm"
                              component="input"
                              type="radio"
                              id="workPhoneComm1"
                              className="radio-custom"
                              value={1}
                              checked={ this.state.workPhoneComm == "1" ? true : false }
                              onChange={ (e) => this.setState({ workPhoneComm: e.target.value }) }
                            />
                            <label htmlFor="workPhoneComm1" className="radio-custom-label">Do not contact me at Home</label>
                          </div>
                          <div>
                            <Field
                              name="workPhoneComm"
                              component="input"
                              type="radio"
                              id="workPhoneComm2"
                              className="radio-custom"
                              value={2}
                              checked={ this.state.workPhoneComm == "2" ? true : false }
                              onChange={ (e) => this.setState({ workPhoneComm: e.target.value }) } />
                            <label htmlFor="workPhoneComm2" className="radio-custom-label">Leave message with department/office name and call-back number on answering maching</label>
                          </div>
                        </div>
                      </div>
                    </Col>
                  </div>
                </Col>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <div className="patient_dv">
                    <h2 className="sub_head">Patient Program</h2>
                  </div>
                  <Col sm="12">
                    <div className="first_tab">
                      <Col sm="4">
                        <div className="txtbx1">
                          <div className="col-sm">
                            <Field name="Program_fk" emptyValue="Select Program"
                              templateList={masterProgram}
                              component={renderSelectField}
                              label="Program"
                              // validate={[ required('Program')  ]}
                              classes={classes}
                            />
                          </div>
                        </div>
                      </Col>
                      <Col sm="4">
                        <div className="txtbx3">
                          <div className="col-sm">
                            <Field name="ProgramStartDate" type="text"
                              component={renderDateField}
                              label="Program Start Date" placeholderText="dd/mm/yyyy"
                              selected={this.state.programStartDate}
                              onDateChange={this.handleProgramChange}
                              classes={classes}
                              showMonthFlag={true}
                              // validate={[ required('DOB2')  ]}
                            />
                          </div>
                        </div>
                      </Col>
                      <Col sm="4">
                        <div className="txtbx1">
                          <div className="col-sm">
                            <Field name="ServiceRequested_fk" emptyValue="Select Service"
                              templateList={masterService}
                              component={renderSelectField}
                              label="Service Requested"
                              // validate={[ required('Service')  ]}
                              classes={classes}
                            />
                          </div>
                        </div>
                      </Col>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <div className="patient_dv">
                    <h2 className="sub_head">Referral Source</h2>
                  </div>
                  <Col sm="12">
                    <div className="first_tab">
                      <Col sm="4">
                        <div className="txtbx1">
                          <div className="col-sm">
                            <Field name="Referral_fk" emptyValue="Select Referral"
                              templateList={masterReferral}
                              component={renderSelectField}
                              label="Referral"
                              // validate={[ required('Referral')  ]}
                              classes={classes}
                            />
                          </div>
                        </div>
                      </Col>
                      <Col sm="4">
                        <div className="txtbx1">
                          <div className="col-sm">
                            <Field name="ReferralReason" type="text"
                              component={renderField}
                              label="Reason For Referral" plaeholder="Last Name"
                              // validate={[ required('Reason For Referral')  ]}
                              classes={classes}
                              maxlength="100"
                            />
                          </div>
                        </div>
                      </Col>
                      <Col sm="4">
                        <div className="txtbx3">
                          <div className="col-sm">
                            <Field name="ReferralDate" type="text"
                              component={renderDateField}
                              label="Date of Referral" placeholderText="dd/mm/yyyy"
                              showMonthFlag={true}
                              selected={this.state.referraldate}
                              onDateChange={this.handleReferralChange}
                              classes={classes}
                              // validate={[ required('Referral date')  ]}
                              showYearFlag={false}
                              maxDateVal={moment()}
                            />
                          </div>
                        </div>
                      </Col>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="txtbx1">
                  <div className="check_patient">
                    <FormGroup check>
                      <Label check>
                        <Input name="AddClientToCaseLoad"
                          checked={this.state.addClientCaseLoad ? "checked" : ""}
                          onChange={(e) => this.setState({ addClientCaseLoad: this.state.addClientCaseLoad == true ? false : true })} type="checkbox" />{" "}
                                        Add client to my caseload
                      </Label>
                    </FormGroup>
                  </div>
                </div>
              </Col>
            </Row>
            <div className="bottom_dv">
              <div className="bottom_dv_inner">
                <Button color="success" type="submit" className="patient_btn">
                  {this.state.patientID_fk ? "UPDATE PATIENT" : "CREATE NEW PATIENT"}
                </Button>
                <Button color="success" onClick={() => browserHistory.push("/patient/list")} className="cancel_btn">CANCEL</Button>
              </div>
            </div>
            {this.state.patient_created ? "Patient created Successfully." : null}
          </form>
          {this.props.loading ? <img className="loading_img" src={require("./../../../assets/images/loading.gif")} /> : ""}
        </div>

      );
    }
}

const mapStateToProps = (state) => ({
  master_data: state.patient.master_data,
  ispatientAvailable: state.patient.ispatientAvailable,
  patient_created: state.patient.patient_created,
  patient_data: state.patient.patient_data,
  initialValues: state.patient.patient_data, // {firstName: 'PuneetBh', lastName:"Bhandari"},
  loading: state.patient.loading,
});

Demographic = reduxForm({
  form: "Demographic", // a unique identifier for this form
  validate,
  enableReinitialize: true,
  onSubmitFail: (errors) => {
    handleFormErrors(errors)
    ; 
},
  // asyncValidate,
  // asyncBlurFields: [ 'email' ]
})(Demographic);

export default connect(mapStateToProps, {
  fetchMasterTablesData, checkPatientExist,
  insertUpdatePatientData, getPatientData }
)(Demographic);

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Field, reduxForm, reset } from "redux-form";
import { Button, Card, CardText, CardTitle,Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Form, FormGroup, Input, Label, Nav, NavItem,NavLink, Row, TabContent,TabPane } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { renderAsyncSelectField, renderDateField, renderField, renderSelectField, renderTextArea } from "app/reduxForm/components/";
import { email, maxLength15, number, onlyLetters, required, requiredDropdown, tooOld } from "app/reduxForm/validations";
import { checkPatientExist, fetchMasterTablesData, getPatientData, insertUpdatePatientData, UpdatePatientData } from "./../../actions";
import { COMM_PREFRENCES, FETCH_PATIENT, PATIENT_TITLE } from "./../../constants";
import { calculateAge, handleFormErrors } from "utils";
import Alert from "react-s-alert";
import { browserHistory, Link } from "react-router";

import Select from "react-select";
// import 'react-select/dist/react-select.css';


const validate = (values) => {
  const errors = {};

  if (!values.dob) {
    // errors.dob = 'Please enter DOB.'
  }
  return errors;
};

class SoapNote extends Component {

    static propTypes = {
      fetchMasterTablesData: PropTypes.func.isRequired,
      checkPatientExist: PropTypes.func.isRequired,
      insertUpdatePatientData: PropTypes.func.isRequired,
    }

    constructor(props) {
      super(props);

      this.state = {
        startDate: "",
        master_data: { masterCPT: [], masterLocation: [], provider: [] },
        loading: false, error: false,
        patientID_fk: localStorage.getItem("patientID_fk"),
        backspaceRemoves: true,
        patient_info: null,
      };
      this.handleChange = this.handleChange.bind(this);
      this.onChange = this.onChange.bind(this);
      this.toggleBackspaceRemoves = this.toggleBackspaceRemoves.bind(this);
    }

    componentWillMount() {

      // this.props.reset('Demographic')
      this.props.fetchMasterTablesData({ "masterdata": "mastercpt,masterlocation,masterprovider" });
      if (this.state.patientID_fk != undefined
            && Object.keys(this.state.patient_data).length == 0
      ) {
        // console.log("here in demogra==  ", this.state)
        // this.props.getPatientData(this.state.patientID_fk);
      }
    }

    componentWillReceiveProps(nextProps) {
      // console.log("next props", nextProps);
      if (nextProps.master_data && this.props.master_data !== nextProps.master_data) {
        this.setState({
          master_data: nextProps.master_data,
          ispatientAvailable: nextProps.ispatientAvailable,
          loading: nextProps.loading,
        });
      }
    }

    handleChange(date) {
      this.setState({
        startDate: date,
        dob: moment(date).format("YYYY-MM-DD"),
      });
    }

    onSubmitubmitHandler(formProps) {

    }

    toggleBackspaceRemoves() {
      this.setState({
        backspaceRemoves: !this.state.backspaceRemoves,
      });
    }

    getUsers(input) {
      if (!input) {
        return Promise.resolve({ options: [] });
      }

      return fetch(`${FETCH_PATIENT}?filter[searchkey]=like:${input}&page[size]=5`)
        .then((response) => response.json())
        .then((json) => {
          let allPatients = [];
          json.data.map((patient, index) => {

            allPatients.push({
              id: patient.attributes.PatientID,
              login: patient.attributes.FirstName,
              dob: patient.attributes.DOB,
              gender: patient.attributes.Gender,
            });
          });
          return { options: allPatients };
        });
    }

    onChange(value) {
      console.log("onchange===", value);
      this.setState({
        patient_name: value,
      });
      let patient_age = calculateAge(value.dob);
      this.setState({
        patient_info: `Age of Patient: ${patient_age} years old       Sex: ${value.gender}`,
      });
    }

    render() {
      // console.log("new sate",this.state)
      const { handleSubmit, pristine, reset, submitting, onSubmitSuccess } = this.props;
      const { classes } = this.props;
      const { provider, masterLocation, masterCPT } = this.state.master_data;

      return (
        <div className={classes.wpad_1 + " " + classes.fl_left_back}>
          <div className={classes.patient_dv1}>
            <h2 className={classes.patient_dv}>Patient Listing</h2>
          </div>
          <form onSubmit={handleSubmit(this.onSubmitubmitHandler.bind(this))}>
            <Row>
              <Col sm="12">
                <div className={classes.first_tab}>
                  <Col sm="4">
                    <div className={classes.txtbx3}>
                      <div className="col-sm">
                        <Field name="dob" type="text"
                          component={renderDateField}
                          label="Date and time of Visit" placeholderText="dd/mm/yyyy"
                          selected={this.state.startDate}
                          onDateChange={this.handleChange}
                          showYearFlag={true}
                          showMonthFlag={true}
                          classes={classes}
                          // validate={[ required('DOB')  ]}
                          maxDateVal={moment()}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className={classes.txtbx3}>
                      <div className="col-sm">
                        <Field name="providerId_fk" emptyValue="Select Staff"
                          templateList={provider}
                          component={renderSelectField}
                          label="Seen By"
                          validate={[required("Staff") ]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className={classes.first_tab}>
                  <Col sm="4">
                    <div className={classes.txtbx3}>
                      <div className="col-sm">
                        {/* <Field name="patientID_fk" 
                                                onChangeSelect={this.onChange}
                                                loadOptions={this.getUsers} 
                                                selectedValue={this.state.patient_name}
                                                component={renderAsyncSelectField} 
                                                label="Seen By"
                                                validate={[ required('Patient')  ]}
                                                classes={classes}
                                            />*/}
                        <Select.Async onChange={this.onChange} valueKey="id" labelKey="login" loadOptions={this.getUsers} value={this.state.patient_name} backspaceRemoves={this.state.backspaceRemoves} />
                      </div>
                    </div>
                  </Col>
                  <Col sm="8">
                    <div className={classes.txtbx3}>
                      <div className="col-sm">
                        <div className={classes.patient_detail}>
                          {this.state.patient_info}
                        </div>
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className={classes.first_tab}>
                  <Col sm="12">
                    <div className={classes.txtbx3}>
                      <label>Patient Diagnosis (ICD 9/10)</label>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className={classes.first_tab}>
                  <Col sm="4">
                    <div className={classes.txtbx3}>
                      <div className="col-sm">
                        <Field name="cptiD_fk" emptyValue="Select Service Code"
                          templateList={masterCPT}
                          component={renderSelectField}
                          label="Select Service / CPT Code"
                          // validate={[ required('Staff')  ]}
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className={classes.first_tab}>
                  <Col sm="4">
                    <div className={classes.txtbx3}>
                      <div className="col-sm">
                        <Field name="locationID_fk" emptyValue="Select Service Location"
                          templateList={masterLocation}
                          component={renderSelectField}
                          label="Select Service Location"
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className={classes.first_tab}>
                  <Col sm="12">
                    <div className={classes.txtbx3}>
                      <div className="col-sm">
                        <Field name="subjective"
                          component={renderTextArea}
                          label="Subjective"
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className={classes.first_tab}>
                  <Col sm="12">
                    <div className={classes.txtbx3}>
                      <div className="col-sm">
                        <Field name="objective"
                          component={renderTextArea}
                          label="Objective"
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className={classes.first_tab}>
                  <Col sm="12">
                    <div className={classes.txtbx3}>
                      <div className="col-sm">
                        <Field name="assessment"
                          component={renderTextArea}
                          label="Assessment"
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className={classes.first_tab}>
                  <Col sm="12">
                    <div className={classes.txtbx3}>
                      <div className="col-sm">
                        <Field name="plans"
                          component={renderTextArea}
                          label="Plan"
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <div className={classes.bottom_dv}>
              <div className={classes.bottom_dv_inner}>
                <Button color="success" type="submit" className={classes.patient_btn}>
                  {this.state.patientID_fk ? "UPDATE PATIENT" : "CREATE NEW PATIENT"}
                </Button>
                <Button color="success" onClick={() => browserHistory.push("/patient/list")} className={classes.cancel_btn}>CANCEL</Button>
              </div>
            </div>
            {this.state.patient_created ? "Patient created Successfully." : null}
          </form>
          {this.state.loading ? <img className={classes.loading_img} src={require("assets/images/loading.gif")} /> : ""}
        </div>

      );
    }
}

const mapStateToProps = (state) => ({
  master_data: state.patient.master_data,
  initialValues: state.patient.patient_data,
  loading: state.patient.loading,
});

SoapNote = reduxForm({
  form: "SoapNote",
  validate,
  enableReinitialize: true,
  onSubmitFail: (errors) => { 
handleFormErrors(errors);
 },
})(SoapNote);

export default connect(mapStateToProps, {
  fetchMasterTablesData, checkPatientExist,
  insertUpdatePatientData, getPatientData }
)(SoapNote);

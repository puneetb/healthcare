import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Field, reduxForm, reset } from "redux-form";
import { Button, Col, Form, Row } from "reactstrap";
import Loader from "./../common/loader";
import RenderList from "./../common/renderList";
import Alert from "react-s-alert";
import HeaderProfile from "./../common/HeaderProfile";
import { browserHistory, Link } from "react-router";
import ModalForm from "./modal";
import { deleteFamilyHistory, fetchHistoryById, getPatientMedicalHistoryList } from "./../../actions";
import { formatDate } from "utils";
import { DEFAULT_PAGE_SIZE } from "app/globalConstants";

class FamilyHistory extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      patientID_fk: localStorage.getItem("patientID_fk"),
      listColums: [],
      historyList: [],
      page_size: DEFAULT_PAGE_SIZE,
    };
    this.handleChange = this.handleChange.bind(this);
    this.loadData = this.loadData.bind(this);
    this.deleteRow = this.deleteRow.bind(this);
    this.editHistoryRow = this.editHistoryRow.bind(this);
  }


  componentWillMount() {
    if (this.state.patientID_fk === null) {
      browserHistory.push("/patient");
    }

    // fetch history data
    this.loadData();

    this.setState({
      listColums: [
        { "key": "FirstName", "label": "First name", "sortingType": "char" },
        { "key": "LastName", "label": "Last name", "sortingType": "char" },
        { "key": "RelationshipName", "label": "Relationship", "sortingType": "char" },
        { "key": "DOB", "label": "Date of Birth", "sortingType": null },
        { "key": "Disease", "label": "Disease", "sortingType": "char" },
      ],
      columnKeys: ["FirstName", "LastName", "Relationship", "DOB", "Disease"],
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ loading: nextProps.loading });
    if (nextProps.historyList
    // && this.props.historyList !== nextProps.historyList 
    ) {
      const formattedData = [];
      // console.log("receiveprops=>>>", nextProps.historyList)
      if (nextProps.historyList.data.length > 0) {
        nextProps.historyList.data.map((historyObj, index) => {
          formattedData.push({
            FirstName: historyObj.attributes.FirstName,
            LastName: historyObj.attributes.LastName,
            Relationship: historyObj.attributes.RelationshipName,
            DOB: formatDate(historyObj.attributes.DOB),
            Disease: historyObj.attributes.Disease,
            id: historyObj.id,
          });
        });
        formattedData.meta = {
          total_pages: nextProps.historyList.meta["total-pages"],
          total_records: nextProps.historyList.meta["total-records"],
          showEditButton: true,
          showDeleteButton: true,
        };
      }
      this.setState({ historyList: formattedData });
    }
  }


  loadData(sort_fields, page_number) {

    let searchObj = {
      page_number: page_number == undefined ? 1 : page_number,
      sort_fields: sort_fields ? sort_fields : null,
      filters: { patientid_fk: this.state.patientID_fk },
      includes: "include=masterrelationship,mastergender,mastericd",
      page_size: this.state.page_size,
    };
    this.props.getPatientMedicalHistoryList(searchObj);
  }

  handleChange(date) {
    this.setState({
      startDate: date,
      dob: moment(date).format("YYYY-MM-DD"),
    });
  }

  deleteRow(history_id) {
    this.props.deleteFamilyHistory(history_id, this.state.patientID_fk);
  }

  editHistoryRow(history_id) {
    this.props.fetchHistoryById(history_id, this.state.patientID_fk);
  }

  render() {
    // console.log("index.js>>", this.state)
    const { classes } = this.props;
    const { loading, historyList } = this.state;
    return (
      <div className="wpad_1  classes.fl_left_back">
        <div className="patient_dv1">
          <h2 className="patient_dv">Patient Profile</h2>
        </div>
        <div className="bottom_con">
          <HeaderProfile classes={classes} patient_id={this.state.patientID_fk} />
          <h3>Family Medical History</h3>
          {historyList.length > 0 ? (
            <RenderList classes={classes} columns={this.state.listColums} tableData={this.state.historyList} columnKeys={this.state.columnKeys} callApi={this.loadData} deleteRow={this.deleteRow} editHistoryRow={this.editHistoryRow} />
          ) : "Nothing to display"}

          <h3>Add Medical History</h3>

          <ModalForm classes={classes} patient_id={this.state.patientID_fk} />
          { loading && <Loader /> }
        </div>
      </div>

    );
  }
}

const mapStateToProps = (state) => ({
  master_data: state.patient.master_data,
  loading: state.patient.loading,
  historyList: state.patient.historyList,
});

export default connect(mapStateToProps, { getPatientMedicalHistoryList, deleteFamilyHistory, fetchHistoryById })(FamilyHistory);

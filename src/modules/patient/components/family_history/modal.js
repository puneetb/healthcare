import React, { Component } from "react";
import { Field, reduxForm, reset } from "redux-form";
import { connect } from "react-redux";
import { Button, Col, Input, Label, Modal, Row } from "reactstrap";
import { renderAsyncSelectField, renderCheckBox, renderField, renderNewDateField, renderSelectField, renderTextArea } from "app/reduxForm/components/";
import { email, maxLength15, number, onlyLetters, required, requiredDropdown, tooOld } from "app/reduxForm/validations";
import DatePicker from "react-datepicker";
import moment from "moment";
import { clearForm, fetchMasterTablesData, saveFamilyHistory } from "./../../actions";
import { calculateAge, handleFormErrors } from "utils";

const validate = (values) => {
  const errors = {};

  if (!/^[0-9]+$/.test(values.AgeOfDiagnosis) || values.AgeOfDiagnosis > 110) {
    errors.AgeOfDiagnosis = "Please enter valid age";
  }

  if (!values.DOB) {
    errors.DOB = "Please enter DOB";
  }

  if (values.DOB && values.AgeOfDiagnosis) {
    let age = calculateAge(moment(values.DOB).format("YYYY-MM-DD"));
    if (age.indexOf("ears") != -1 && values.AgeOfDiagnosis > parseInt(age.match(/\d+/)[0])) {
      // errors.AgeOfDiagnosis = "Age of Diagnosis should be less than or equal to DOB."
    }
  }
  return errors;
};


class FamilyHistoryForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      master_data: { masterICD: [], masterRelationship: [] },
      masterPatientStatus: [{ id: true, value: "Active" }, { id: false, value: "Inactive" }],
      loading: false,
      patient_id: props.patient_id,
      Gender_fk: "1",
      DOB: "",
      DateOfDeath: "",
      family_history: {},
      death_cause_disable: true,
      death_date_disabled: true,
      history_id: null,
    };
    this.handleChangeDOB = this.handleChangeDOB.bind(this);
    this.handleChangeDeathDate = this.handleChangeDeathDate.bind(this);
  }

  componentWillMount() {

  }


  onSubmitubmitHandler(formProps) {
    formProps.DOB = this.state.DOB != "" ? moment(this.state.DOB).format("YYYY-MM-DD") : null;
    formProps.DateOfDeath = this.state.DateOfDeath != "" ? moment(this.state.DateOfDeath).format("YYYY-MM-DD") : null;
    formProps.PatientID_fk = this.state.patient_id;
    formProps.Gender_fk = this.state.Gender_fk;
    formProps.createdBy_fk = 1;
    formProps.id = this.state.history_id != null ? this.state.history_id : null;

    this.props.saveFamilyHistory(formProps);
    this.props.clearForm();
  }


  handleChangeDOB(date, event) {
    event.onChange(moment(date).format("YYYY-MM-DD"));
    this.setState({
      DOB: date,
      DateOfDeath: "",
      death_date_disabled: false,
    });
  }
  handleChangeDeathDate(date, event) {
    event.onChange(moment(date).format("YYYY-MM-DD"));
    this.setState({
      DateOfDeath: date,
      death_cause_disable: false,
    });
  }

  componentWillReceiveProps(nextprops) {
    const { master_data, initialValues } = nextprops;
    if (master_data && this.props.master_data !== master_data) {
      this.setState({
        master_data: master_data,
      });
    }

    if (initialValues && this.props.initialValues !== initialValues) {
      this.setState({
        DOB: moment(initialValues.DOB),
        DateOfDeath: initialValues.DateOfDeath ? moment(initialValues.DateOfDeath) : "",
        death_cause_disable: initialValues.CauseOfDeath != null ? false : true,
        death_date_disabled: initialValues.DateOfDeath != null ? false : true,
        history_id: initialValues.MedicalFamilyHistoryId,
      });
    }
  }

  render() {
    // console.log("new state: ", this.state);
    const { handleSubmit, pristine, reset, submitting, classes, fieldDisabled } = this.props;
    const { masterRelationship, masterICD } = this.state.master_data;
    const { masterPatientStatus } = this.state;
    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmitubmitHandler.bind(this))}>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="FirstName"
                        type="text" component={renderField}
                        label="First Name"
                        plaeholder="First Name"
                        classes={classes}
                        validate={[required("First Name")]}
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="LastName"
                        type="text" component={renderField}
                        label="Last Name"
                        plaeholder="Last Name"
                        disabled={true}
                        classes={classes}
                        validate={[required("Last Name")]}
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <label>Gender</label>
                      <div className="radio_dv"><Field
                        name="Gender_fk"
                        component="input"
                        type="radio"
                        id="radio-1"
                        className="radio-custom"
                        value="1"
                        checked={ this.state.Gender_fk == "1" ? true : false }
                        onClick={ () => this.setState({ Gender_fk: "1" }) } />
                      <label htmlFor="radio-1" className="radio-custom-label">Male</label>
                      <Field
                        name="Gender_fk"
                        component="input"
                        type="radio"
                        id="radio-2"
                        className="radio-custom"
                        value="2"
                        checked={ this.state.Gender_fk == "2" ? true : false }
                        onClick={ () => this.setState({ Gender_fk: "2" }) } />
                      <label htmlFor="radio-2" className="radio-custom-label">Female</label>
                      <Field
                        name="Gender_fk"
                        component="input"
                        type="radio"
                        id="radio-3"
                        className="radio-custom"
                        value="3"
                        checked={ this.state.Gender_fk == "3" ? true : false }
                        onClick={ () => this.setState({ Gender_fk: "3" }) } />
                      <label htmlFor="radio-3" className="radio-custom-label">Other</label></div>
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx3">
                    <div className="col-sm">
                      <Field name="DOB" type="text"
                        component={renderNewDateField}
                        label="DOB" placeholderText="dd/mm/yyyy"
                        selected={this.state.DOB}
                        onDateChange={this.handleChangeDOB}
                        showYearFlag={true}
                        showMonthFlag={true}
                        classes={classes}
                        maxDateVal={moment()}
                        props={this.props}
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="RelationshipID_fk" emptyValue="Relationship"
                        templateList={masterRelationship}
                        component={renderSelectField}
                        label="Relationship"
                        classes={classes}
                        validate={[required("Relationship")]}
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx3">
                    <div className="col-sm">
                      <Field name="AgeOfDiagnosis"
                        type="text" component={renderField}
                        label="Age of Diagnosis(Years)"
                        plaeholder="34"
                        classes={classes}
                        validate={[required("Age of Diagnosis"), number]}
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="Disease_fk" emptyValue="Select Disease"
                        templateList={masterICD}
                        component={renderSelectField}
                        label="Disease"
                        classes={classes}
                        validate={[required("Disease")]}
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx3">
                    <div className="col-sm">
                      <Field name="IsActive" emptyValue="Select Status"
                        templateList={masterPatientStatus}
                        component={renderSelectField}
                        label="Disease Status"
                        validate={[required("Disease Status")]}
                        classes={classes}
                        validate={[required("Disease Status")]}
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx3">
                    <div className="col-sm">
                      <Field name="DateOfDeath" type="text"
                        component={renderNewDateField}
                        label="Date of Death" placeholderText="dd/mm/yyyy"
                        selected={this.state.DateOfDeath}
                        onDateChange={this.handleChangeDeathDate}
                        showYearFlag={true}
                        showMonthFlag={true}
                        classes={classes}
                        disabled={this.state.death_date_disabled}
                        minDateVal={this.state.DOB ? moment(this.state.DOB) : ""}
                        maxDateVal={moment()}
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="12">
                  <div className="txtbx3">
                    <div className="col-sm">
                      <Field name="CauseOfDeath"
                        component={renderTextArea}
                        label="Cause of Death"
                        disabled={this.state.death_cause_disable}
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="12">
                  <div className="txtbx3">
                    <div className="col-sm">
                      <Field name="Observation"
                        component={renderTextArea}
                        label="Observation"
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>

          <div className="bottom_dv">
            <div className="bottom_dv_inner">
              <Button color="success" type="submit" className="patient_btn">Save</Button>
              {/* <Button color="success" onClick={() => browserHistory.push('/patient/list')} className="cancel_btn">CANCEL</Button>*/}
            </div>
          </div>
        </form>
      </div>

    );
  }
}

const mapStateToProps = (state) => ({
  master_data: state.patient.master_data,
  initialValues: state.patient.family_history,
  loading: state.patient.loading,
});

FamilyHistoryForm = reduxForm({
  form: "FamilyHistoryForm", // a unique identifier for this form
  validate,
  enableReinitialize: true,
  onSubmitFail: (errors) => {
    handleFormErrors(errors);
  },
  // asyncValidate,
  // asyncBlurFields: [ 'email' ]
})(FamilyHistoryForm);

export default connect(mapStateToProps, { fetchMasterTablesData, saveFamilyHistory, clearForm })(FamilyHistoryForm);

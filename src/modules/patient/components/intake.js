import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Container } from "reactstrap";
import { Button, Card, CardText, CardTitle,Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Form, FormGroup, Input, Nav, NavItem, NavLink,Row, TabContent, TabPane } from "reactstrap";
import TabSection from "./tabSection";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

class PatientIntake extends Component {

  constructor(props) {

    super(props);
    this.state = {
      activeTab: "1",
      startDate: moment(),
      dropdownOpen: false,
    };
    this.handleChange = this.handleChange.bind(this);
    localStorage.removeItem("address_id");
    localStorage.removeItem("insurance_id");
    localStorage.removeItem("patientID_fk");
    localStorage.removeItem("preference_id");

    document.title = "Patient Intake";
  }

  handleChange(date) {
    this.setState({
      startDate: date,
    });
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }


  render() {
    const { classes } = this.props;

    return (
      <Col xs="3" sm="11" className="wpad_1">
        <div className="right_con1">
          <div className="patient_dv">
            <h2 className="patient_dv">Patient Manager</h2>
          </div>
          <div className="bottom_con">
            <div className="tab_layout">
              <TabSection classes={classes} />
            </div>

            <div className="footer">
              <p><i className="fa fa-copyright " aria-hidden="true"/> 2017 Healthcare. All Right Reserved.</p>
            </div>
          </div>
        </div>
      </Col>
    );
  }
}


export default PatientIntake;

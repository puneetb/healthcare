import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { change, Field, formValueSelector, initialize, reduxForm, reset } from "redux-form";
import { Button, Card, CardText, CardTitle, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Form, FormGroup, Input, Label, Nav, NavItem, NavLink, Row, TabContent, TabPane } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { maskInputField, RadioButton, renderDateField, renderField, renderFieldWithoutValue, renderSelectField, renderUploadField } from "app/reduxForm/components/";
import { email, maxLength, maxLength15, number, onlyLetters, required, requiredDropdown, tooOld } from "app/reduxForm/validations";
import { clearForm, fetchMasterTablesData, getInsuranceData, insertUpdatePatientInsurance } from "./../actions";
import Alert from "react-s-alert";
import { handleFormErrors } from "utils";
import { browserHistory, Link } from "react-router";


const validate = (values) => {
  const errors = {};

  if (!values.dob) {
    // errors.dob = 'Please enter DOB.'
  }

  if (values.title == "0") {
    // errors.title = 'Please enter DOB.'
  }
  // console.log(errors);
  return errors;
};


class InsuranceForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardIssueDateObj: "",
      personSamePatient: false,
      master_data: { insuranceCompanies: [], insurancePlanType: [] },
      patientID_fk: localStorage.getItem("patientID_fk"),
      insurance_data: {},
      insurance_id: localStorage.getItem("insurance_id") ? localStorage.getItem("insurance_id") : null,
      loading: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this._handleImageChange = this._handleImageChange.bind(this);
  }

  componentWillMount() {
    this.props.clearForm();
    if (this.state.insurance_id != null) {
      this.props.getInsuranceData(this.state.insurance_id);
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }


  onSubmitubmitHandler(formProps) {
    formProps.id = this.state.insurance_id;
    formProps.CardIssueDate = this.state.cardIssueDate;
    formProps.patientID_fk = this.state.patientID_fk;
    formProps.base64Front = this.state.insurancePhotoPathFront_url != null && this.state.insurancePhotoPathFront_url.indexOf("data:image") > -1 ? this.state.insurancePhotoPathFront_url : null,
    formProps.insurancePhotoPathBack = "";
    formProps.insurancePhotoPathFront = "";
    formProps.base64Back = this.state.insurancePhotoPathBack_url != null && this.state.insurancePhotoPathBack_url.indexOf("data:image") > -1 ? this.state.insurancePhotoPathBack_url : null,
    formProps.isActive = true;
    formProps.insurancePersonSameAsPatient = this.state.personSamePatient;
    formProps.createdBy_fk = 1;
    this.props.insertUpdatePatientInsurance(formProps);
  }


  handleChange(date) {
    this.setState({
      cardIssueDateObj: date,
      cardIssueDate: moment(date).format("YYYY-MM-DD"),
    });
  }

  return_age() {
    return (
      <div>Age</div>
    );
  }

  componentWillReceiveProps({ master_data, insurance_updated, insurance_data, loading }) {

    if (master_data != undefined) {
      this.setState({
        master_data: master_data,
      });
    }

    this.setState({
      loading: loading,
    });

    if (insurance_updated) {
      window.scrollTo(0, 0);
    }

    if (insurance_data && insurance_data !== this.state.insurance_data) {
      this.setState({
        insurance_data: insurance_data,
        cardIssueDateObj: insurance_data.cardIssueDate != null ? moment(insurance_data.cardIssueDate) : null,
        cardIssueDate: insurance_data.cardIssueDate != null ? moment(insurance_data.cardIssueDate).format("YYYY-MM-DD") : null,
        insurancePhotoPathFront_url: insurance_data.insurancePhotoPathThumbFront,
        insurancePhotoPathBack_url: insurance_data.insurancePhotoPathThumbBack,
        personSamePatient: insurance_data.insurancePersonSameAsPatient,
      });
    }
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    let preview_url = e.target.name + "_url";

    reader.onloadend = () => {
      this.setState({
        file: file,
        [e.target.name + "_url"]: reader.result,
      });
    };

    reader.readAsDataURL(file);
  }

  render() {
    console.log("new state: ", this.props.loading);
    const { handleSubmit, pristine, reset, submitting } = this.props;
    const { classes } = this.props;
    const { insuranceCompanies, insurancePlanType } = this.state.master_data;

    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmitubmitHandler.bind(this))}>

          <Row>
            <Col sm="12">
              <Label check className="patient_check classes.insure_pat">
                <Input name="insurancePersonSameAsPatient" onChange={(e) => this.setState({ personSamePatient: this.state.personSamePatient == true ? false : true })}
                  checked={this.state.personSamePatient} type="checkbox" />{" "}
                            Insured Person is the same person as the Patient
              </Label>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="insuranceCompany_fk" emptyValue="Select Company"
                        templateList={insuranceCompanies}
                        component={renderSelectField}
                        validate={[required("Company")]}
                        label="Insurance Company"
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="insuranceCompanyAddress"
                        type="text" component={renderField}
                        label="Insurance Company Address"
                        plaeholder="PO Box 981106, EL Paso, TX 79998"
                        classes={classes}
                        validate={[required("Insurance Company Address")]}
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="carrierPayerID"
                        type="text" component={renderField}
                        label="Carrier Payer ID"
                        plaeholder="123"
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="tplCode"
                        type="text" component={renderField}
                        label="TPL Code"
                        plaeholder="TP123"
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="insuranceIDNumber"
                        type="text" component={renderField}
                        label="Insurance ID Number"
                        validate={[required("Insurance ID Number")]}
                        plaeholder="43543"
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="insuranceGroupName"
                        type="text" component={renderField}
                        label="Insurance Group Name"
                        plaeholder="Grtoup name"
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>

          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="insurancePlanName"
                        type="text" component={renderField}
                        label="Insurance Plan Name"
                        plaeholder="Plan Name"
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="insurancePlanType_fk" emptyValue="Insurance Plan Type"
                        templateList={insurancePlanType}
                        component={renderSelectField}
                        label="Insurance Plan Type"
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="insuranceClaimOfficeNumber"
                        type="text" component={renderField}
                        label="Insurance Claim Office Number"
                        plaeholder="Office"
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>

          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="visitsAllowedPerYear"
                        type="text" component={renderField}
                        label="Number Visits Allowed Per Year"
                        plaeholder="12"
                        classes={classes}
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx3">
                    <div className="col-sm">
                      <Field name="cardIssueDate" type="text"
                        component={renderDateField}
                        label="Card Issue Date" placeholderText="dd/mm/yyyy"
                        selected={this.state.cardIssueDateObj}
                        onDateChange={this.handleChange}
                        showYearFlag={true}
                        showMonthFlag={true}
                        classes={classes}
                        maxDateVal={moment()}
                      />
                    </div>
                  </div>
                </Col>

              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field
                        name="insurancePhotoPathFront" type="file" component= {renderUploadField}
                        onChange={this._handleImageChange}
                        label="Insurance Photo Front"
                      />
                    </div>
                    {this.state.insurancePhotoPathFront_url ? (<img src={this.state.insurancePhotoPathFront_url} width="100" />) : ""}
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field
                        name="insurancePhotoPathBack" type="file" component= {renderUploadField}
                        onChange={this._handleImageChange}
                        label="Insurance Photo Back"
                      />
                    </div>
                    {this.state.insurancePhotoPathBack_url ? (<img src={this.state.insurancePhotoPathBack_url} width="100" />) : ""}
                  </div>
                </Col>
              </div>
            </Col>
          </Row>

          <div className="bottom_dv">
            <div className="bottom_dv_inner">
              <Button color="success" type="submit" className="patient_btn">ADD INSURANCE DETAILS</Button>
              <Button color="success" onClick={() => browserHistory.push("/patient/list")} className="cancel_btn">CANCEL</Button>
            </div>
          </div>
        </form>
        {this.props.loading ? <img className="loading_img" src={require("./../../../assets/images/loading.gif")} /> : ""}
      </div>

    );
  }
}

const mapStateToProps = (state) => ({
  insurance_updated: state.patient.insurance_updated,
  master_data: state.patient.master_data,
  insurance_data: state.patient.insurance_data,
  initialValues: state.patient.insurance_data,
  loading: state.patient.loading,
});

InsuranceForm = reduxForm({
  form: "InsuranceForm", // a unique identifier for this form
  validate,
  enableReinitialize: true,
  onSubmitFail: (errors) => {
    handleFormErrors(errors) ;
  },
  // asyncValidate,
  // asyncBlurFields: [ 'email' ]
})(InsuranceForm);

export default connect(mapStateToProps, { fetchMasterTablesData, insertUpdatePatientInsurance, getInsuranceData, clearForm })(InsuranceForm);

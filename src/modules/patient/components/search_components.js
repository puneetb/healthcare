import React from "react";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import InputMask from "react-input-mask";
import { Button, Card, CardText, CardTitle,Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Form, FormGroup, Input, Label, Nav, NavItem,NavLink, Pagination, PaginationItem,PaginationLink ,Row , TabContent, Table, TabPane } from "reactstrap";
import { Field, reduxForm, reset } from "redux-form";
import { maskInputField, RadioButton, renderDateField, renderField, renderSelectField, renderUploadField } from "app/reduxForm/components/";
import { email, maxLength15, number, onlyLetters, required, requiredDropdown, tooOld } from "app/reduxForm/validations";

export const SimpleSearch = ({ classes, onClickBtn }) => (
  <div className="inner_frst_listing">
    <Row>
      <Col sm="4">
        <div className="top_pat_head">
          <h2>Search<span> ( enter any part of first or last name )</span></h2>
        </div>
      </Col>
      <Col sm="4">
        <div className="label_list">
          <Field name="keyword" type="text" component={renderField}
            plaeholder="eg. John Smith"
            validate={[required("Search Keyword") ]}
            // value={this.state.symbol}
            classes={classes}
          />
        </div>
      </Col>
      <Col sm="4">
        <Button color="success" className="cancel_btn">Search</Button>
        <Button color="success"
          onClick={onClickBtn}
          className="cancel_btn"
        >Advance Search</Button>
      </Col>
    </Row>
  </div>
);

export const AdvanceSearch = ({ pristine, submitting, reset, classes, masterPatientStatus, genderState, onChangeGender, created_from_value, handleCreatedFromDateChange, created_to_value, handleCreatedToDateChange, dob_to_value, dob_from_value, handleDobFromDateChange, handleDobToDateChange, onHideAdvance }) => (

  <div className="inner_frst_listing">
    <Row>
      <div className="first_tab">
        <Col sm="8">
          <div className="top_pat_head">
            <h2>Search<span> ( enter any part of first or last name )</span></h2>
          </div>
        </Col>
        <Col sm="4">
          <div className="back_to_search">
            <a href="javascript:void(0)" onClick={onHideAdvance}>
                         Back to Search
            </a>
          </div>
        </Col>
      </div>
      <Col sm="12">
        <div className="first_tab">
          <Col sm="4">
            <div className="txtbx3">
              <div className="col-sm">
                <Field name="firstName" type="text" component={renderField}
                  label="First Name" plaeholder="First Name"
                  validate={[onlyLetters ]}
                  classes={classes}
                />
              </div>
            </div>
          </Col>
          <Col sm="4">
            <div className="txtbx1">
              <div className="col-sm">
                <Field name="middleName" type="text" component={renderField}
                  label="Middle Name" plaeholder="Middle Name"
                  validate={[onlyLetters]}
                  classes={classes}
                />
              </div>
            </div>
          </Col>
          <Col sm="4">
            <div className="txtbx1">
              <div className="col-sm">
                <Field name="lastName" type="text" component={renderField}
                  label="Last Name" plaeholder="Last Name"
                  validate={[onlyLetters]}
                  classes={classes}
                />
              </div>
            </div>
          </Col>
        </div>
      </Col>
    </Row>
    <Row>
      <Col sm="12">
        <div className="first_tab">
          <Col sm="4">
            <div className="txtbx3">
              <div className="col-sm">
                <Field name="ssn"
                  component={maskInputField}
                  placeholder="122-22-12345"
                  maskFormat="999-99-9999"
                  label="SSN"
                  classes={classes}
                />
              </div>
            </div>
          </Col>
          <Col sm="4">
            <div className="txtbx3">
              <div className="col-sm">
                <Field name="clientStatus_fk" emptyValue="Select Status"
                  templateList={masterPatientStatus}
                  component={renderSelectField}
                  label="Client Status"
                  classes={classes}
                />
              </div>
            </div>
          </Col>
          <Col sm="4">
            <div className="txtbx1">
              <div className="col-sm">
                <label>Gender</label>
                <div className="radio_dv"><Field
                  name="gender_fk"
                  component="input"
                  type="radio"
                  id="radio-1"
                  className="radio-custom"
                  value="Male"
                  // checked={ genderState === 'Male' ? true : false }
                  // onChange={onChangeGender} 
                />
                <label htmlFor="radio-1" className="radio-custom-label">Male</label>
                <Field
                  name="gender_fk"
                  component="input"
                  type="radio"
                  id="radio-2"
                  className="radio-custom"
                  value="Female"
                  // checked={ genderState === 'Female' ? true : false }
                  // onChange={onChangeGender} 
                />
                <label htmlFor="radio-2" className="radio-custom-label">Female</label>
                <Field
                  name="gender_fk"
                  component="input"
                  type="radio"
                  id="radio-3"
                  className="radio-custom"
                  value="other"
                  // checked={ genderState === 'other' ? true : false }
                  // onChange={onChangeGender} 
                />
                <label htmlFor="radio-3" className="radio-custom-label">Other</label></div>
              </div>
            </div>
          </Col>
          <Col sm="12">
            <div className="first_tab">
              <Col sm="4">
                <div className="txtbx3">
                  <div className="col-sm">
                    <Field name="patient_id" type="text" component={renderField}
                      label="Patient ID" plaeholder="Patient ID"
                      validate={[number]}
                      classes={classes}
                    />
                  </div>
                </div>
              </Col>
              <Col sm="4">
                <div className="txtbx3">
                  <div className="col-sm">
                    <Field name="created_from" type="text"
                      component={renderDateField}
                      label="From Created Date" placeholderText="dd/mm/yyyy"
                      selected={created_from_value}
                      onDateChange={handleCreatedFromDateChange}
                      showYearFlag={true}
                      showMonthFlag={true}
                      classes={classes}
                      maxDateVal={moment()}
                    />
                  </div>
                </div>
              </Col>
              <Col sm="4">
                <div className="txtbx3">
                  <div className="col-sm">
                    <Field name="created_to" type="text"
                      component={renderDateField}
                      label="To Created Date" placeholderText="dd/mm/yyyy"
                      selected={created_to_value}
                      onDateChange={handleCreatedToDateChange}
                      showYearFlag={true}
                      showMonthFlag={true}
                      classes={classes}
                      maxDateVal={moment()}
                    />
                  </div>
                </div>
              </Col>
            </div>
          </Col>
          <Col sm="12">
            <div className="first_tab">
              <Col sm="4">
                <div className="txtbx3">
                  <div className="col-sm">
                    <Field name="dob_from" type="text"
                      component={renderDateField}
                      label="From DOB" placeholderText="dd/mm/yyyy"
                      selected={dob_from_value}
                      onDateChange={handleDobFromDateChange}
                      showYearFlag={true}
                      showMonthFlag={true}
                      classes={classes}
                      maxDateVal={moment()}
                    />
                  </div>
                </div>
              </Col>
              <Col sm="4">
                <div className="txtbx3">
                  <div className="col-sm">
                    <Field name="dob_to" type="text"
                      component={renderDateField}
                      label="To DOB" placeholderText="dd/mm/yyyy"
                      selected={dob_to_value}
                      onDateChange={handleDobToDateChange}
                      showYearFlag={true}
                      showMonthFlag={true}
                      classes={classes}
                      maxDateVal={moment()}
                    />
                  </div>
                </div>
              </Col>
              <Col sm="4">
                <div className="txtbx3">
                  <div className="add_pat">

                    <Button color="success">Search</Button>
                    <Button type="button" disabled={pristine || submitting} onClick={reset}>
                                            Reset
                    </Button>

                  </div>
                </div>
              </Col>
            </div>
          </Col>
        </div>
      </Col>
    </Row>
  </div>
);

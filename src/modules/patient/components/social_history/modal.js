import React, { Component } from "react";
import { Field, reduxForm, reset } from "redux-form";
import { connect } from "react-redux";
import { Button, Col, Input, Label, Modal, Row } from "reactstrap";
import { renderAsyncSelectField, renderCheckBox, renderField, renderNewDateField, renderSelectField, renderTextArea } from "app/reduxForm/components/";
import { email, maxLength15, number, onlyLetters, required, requiredDropdown, tooOld } from "app/reduxForm/validations";
import DatePicker from "react-datepicker";
import moment from "moment";
import { clearForm, getPatientSocialHistory, saveSocialHistory } from "./../../actions";
import { calculateAge, handleFormErrors } from "utils";

const validate = (values) => {
  const errors = {};

  return errors;
};


class SocialHistoryForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      master_data: { masterSocialHistory: [], masterTravelHistory: [] },
      loading: false,
      patient_id: props.patient_id,
      family_history: {},
      social_history_id: null,
    };
  }

  componentWillMount() {
    let searchObj = {
      filters: { patientid_fk: this.state.patient_id },
      // includes: 'include=masteralcohal,mastertravel,mastertobacco,masterdrug'
    };
    this.props.getPatientSocialHistory(searchObj);
  }

  onSubmitubmitHandler(formProps) {
    formProps.PatientID_fk = this.state.patient_id;
    formProps.createdBy_fk = 1;
    formProps.id = this.state.history_id != null ? this.state.history_id : null;

    this.props.saveSocialHistory(formProps);
    this.props.clearForm();
  }

  componentWillReceiveProps(nextprops) {
    console.log("next props>>>>>>", nextprops);
    const { master_data, initialValues } = nextprops;
    if (master_data && this.props.master_data !== master_data) {
      this.setState({
        master_data: master_data,
      });
    }

    if (initialValues && this.props.initialValues !== initialValues) {
      this.setState({
        DOB: moment(initialValues.DOB),
        DateOfDeath: initialValues.DateOfDeath ? moment(initialValues.DateOfDeath) : "",
        death_cause_disable: initialValues.CauseOfDeath != null ? false : true,
        death_date_disabled: initialValues.DateOfDeath != null ? false : true,
        history_id: initialValues.MedicalFamilyHistoryId,
      });
    }
  }

  render() {
    // console.log("new state: ", this.state);
    const { handleSubmit, pristine, reset, submitting, classes, fieldDisabled } = this.props;
    const { masterSocialHistory, masterTravelHistory } = this.state.master_data;
    const { masterPatientStatus } = this.state;
    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmitubmitHandler.bind(this))}>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="AlcohalID_fk" emptyValue="Alcohol"
                        templateList={masterSocialHistory}
                        component={renderSelectField}
                        label="Alcohol"
                        validate={ [required("Alcohol")] }
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx3">
                    <div className="col-sm">
                      <Field name="TobaccoID_fk" emptyValue="Tobacco"
                        templateList={masterSocialHistory}
                        component={renderSelectField}
                        label="Tobacco"
                        validate={ [required("Tobacco")] }
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="DrugID_fk" emptyValue="Drugs"
                        templateList={masterSocialHistory}
                        component={renderSelectField}
                        label="Drugs"
                        validate={ [required("Drugs")] }
                      />
                    </div>
                  </div>
                </Col>
                <Col sm="4">
                  <div className="txtbx3">
                    <div className="col-sm">
                      <Field name="Occupation"
                        type="text" component={renderField}
                        label="Occupation"
                        plaeholder="Occupation"
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <div className="first_tab">
                <Col sm="4">
                  <div className="txtbx1">
                    <div className="col-sm">
                      <Field name="TravelID_fk" emptyValue="Travel"
                        templateList={masterSocialHistory}
                        component={renderSelectField}
                        label="Travel"
                        validate={ [required("Travel")] }
                      />
                    </div>
                  </div>
                </Col>
              </div>
            </Col>
          </Row>
          <div className="bottom_dv">
            <div className="bottom_dv_inner">
              <Button color="success" type="submit" className="patient_btn">Save</Button>
              {/* <Button color="success" onClick={() => browserHistory.push('/patient/list')} className="cancel_btn">CANCEL</Button>*/}
            </div>
          </div>
        </form>
      </div>

    );
  }
}

const mapStateToProps = (state) => ({
  master_data: state.patient.master_data,
  initialValues: state.patient.social_history,
  loading: state.patient.loading,
});

SocialHistoryForm = reduxForm({
  form: "SocialHistoryForm", // a unique identifier for this form
  validate,
  enableReinitialize: true,
  onSubmitFail: (errors) => {
    handleFormErrors(errors)
    ;
  },
  // asyncValidate,
  // asyncBlurFields: [ 'email' ]
})(SocialHistoryForm);

export default connect(mapStateToProps, { saveSocialHistory, clearForm, getPatientSocialHistory })(SocialHistoryForm);

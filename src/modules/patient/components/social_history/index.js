import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Field, reduxForm, reset } from "redux-form";
import { Button, Col, Form, Row } from "reactstrap";
import Loader from "./../common/loader";
import HeaderProfile from "./../common/HeaderProfile";
import { browserHistory, Link } from "react-router";
import SocialHistoryForm from "./modal";
import { fetchHistoryById } from "./../../actions";
import { formatDate } from "utils";
import { DEFAULT_PAGE_SIZE } from "app/globalConstants";

class SocialHistory extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      patientID_fk: localStorage.getItem("patientID_fk"),
      listColums: [],
      historyList: [],
      page_size: DEFAULT_PAGE_SIZE,
    };
  }


  componentWillMount() {
    if (this.state.patientID_fk === null) {
      browserHistory.push("/patient");
    }

    // fetch history data
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ loading: nextProps.loading });
    if (nextProps.historyList
    // && this.props.historyList !== nextProps.historyList 
    ) {
      const formattedData = [];
      // console.log("receiveprops=>>>", nextProps.historyList)
      if (nextProps.historyList.data.length > 0) {
        nextProps.historyList.data.map((historyObj, index) => {
          formattedData.push({
            FirstName: historyObj.attributes.FirstName,
            LastName: historyObj.attributes.LastName,
            Relationship: historyObj.attributes.RelationshipName,
            DOB: formatDate(historyObj.attributes.DOB),
            Disease: historyObj.attributes.Disease,
            id: historyObj.id,
          });
        });
        formattedData.meta = {
          total_pages: nextProps.historyList.meta["total-pages"],
          total_records: nextProps.historyList.meta["total-records"],
          showEditButton: true,
          showDeleteButton: true,
        };
      }
      this.setState({ historyList: formattedData });
    }
  }


  render() {
    // console.log("index.js>>", this.state)
    const { classes } = this.props;
    const { loading, historyList } = this.state;
    return (
      <div className="wpad_1  classes.fl_left_back">
        <div className="patient_dv1">
          <h2 className="patient_dv">Patient Profile</h2>
        </div>
        <div className="bottom_con">
          <HeaderProfile classes={classes} patient_id={this.state.patientID_fk} />
          <h3>Family Social History</h3>

          <SocialHistoryForm classes={classes} patient_id={this.state.patientID_fk} />
          { loading && <Loader /> }
        </div>
      </div>

    );
  }
}

const mapStateToProps = (state) => ({
  master_data: state.patient.master_data,
  loading: state.patient.loading,
  historyList: state.patient.historyList,
});

export default connect(mapStateToProps, { fetchHistoryById })(SocialHistory);


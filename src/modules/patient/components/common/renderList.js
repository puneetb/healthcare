import React, { Component } from "react";
import { Button, Col, PaginationItem, PaginationLink, Paginations, Row, Table } from "reactstrap";
import PropTypes from "prop-types";
import Pagination from "./../pagination/Pagination";


class RenderList extends Component {

  static propTypes = {
    // columns: PropTypes.columns.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {
      columns: this.props.columns,
      tableData: this.props.tableData,
      classes: this.props.classes,
      columnKeys: this.props.columnKeys,
      sorting_all_fields: null,
      total_pages: this.props.tableData.meta.total_pages,
      total_records: this.props.tableData.meta.total_records,
      page_number: 1,
      numeric_sorting: "fa-sort-numeric-asc",
      char_sorting: "fa-sort-alpha-asc",
      showEditButton: this.props.tableData.meta.showEditButton,
      showDeleteButton: this.props.tableData.meta.showDeleteButton,
    };
    this.handleSorting = this.handleSorting.bind(this);
    this.getPaginationLinks = this.getPaginationLinks.bind(this);
    this.onClickPagelink = this.onClickPagelink.bind(this);
    this.onEditClick = this.onEditClick.bind(this);
    this.onDeleteClick = this.onDeleteClick.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      tableData: nextProps.tableData,
      total_pages: nextProps.tableData.meta.total_pages,
      total_records: nextProps.tableData.meta.total_records,
    });
  }

  handleSorting(event) {
    const sort_field = event.target.id;


    if (sort_field == "id" || sort_field == "ssn" || sort_field == "dob") {
      this.setState({
        numeric_sorting: this.state.numeric_sorting == "fa-sort-numeric-asc" ? "fa-sort-numeric-desc" : "fa-sort-numeric-asc",
      });
    } else {
      this.setState({
        char_sorting: this.state.char_sorting == "fa-sort-alpha-asc" ? "fa-sort-alpha-desc" : "fa-sort-alpha-asc",
      });
    }

    var foundPositive = this.state.sorting_all_fields === sort_field;
    var foundNegative = this.state.sorting_all_fields === `-${sort_field}`;

    if (foundPositive) {
      this.state.sorting_all_fields = `-${sort_field}`;
    } else if (foundNegative) {
      this.state.sorting_all_fields = sort_field;
    } else {
      this.state.sorting_all_fields = `-${sort_field}`;
    }

    this.props.callApi(this.state.sorting_all_fields);
  }

  renderColumns() {
    const { columns, classes } = this.state;
    let columnKeys = [];
    return columns.map((columnObj, index) => {

      let sort_icon = null;
      let crsrClas = null;
      if (columnObj.sortingType && columnObj.sortingType == "char") {
        sort_icon = this.state.char_sorting;
        crsrClas = "crsr_pointer";
      } else if (columnObj.sortingType && columnObj.sortingType == "number") {
        sort_icon = this.state.numeric_sorting;
        crsrClas = "crsr_pointer";
      }

      return (
        <th className={crsrClas} id={columnObj.key} key={index} onClick={this.handleSorting}>
          {columnObj.label}
          {sort_icon ? (
            <i id={columnObj.key} className={`fa ${sort_icon}`} aria-hidden="true"></i>
          ) : null}
        </th>
      );
    });


  }

  renderActionColumn() {
    if (this.state.showEditButton || this.state.showDeleteButton) {
      return (
        <th>Actions</th>
      );
    }
  }

  showEditDeleteButton() {
    if (this.state.showEditButton || this.state.showDeleteButton) {
      return (
        <th>
          <a href="javascript:void(0)">
            <i className="fa fa-pencil" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i className="fa fa-trash-o" aria-hidden="true"></i>
          </a>
        </th>
      );
    }
  }

  onEditClick(event) {
    let row_id = event.target.id;
    this.props.editHistoryRow(row_id);
  }

  onDeleteClick(event) {
    let row_id = event.target.id;
    this.props.deleteRow(row_id);
  }

  renderList() {
    if (this.state.tableData.length > 0) {

      let objects = this.state.tableData;
      let labels = this.state.columnKeys;
      let thisobj = this;
      return objects.map(function(key1, rowIndex) {
        return <TableRow objects={objects} key={rowIndex} rowIndex={rowIndex} labels={labels} onEditClick={thisobj.onEditClick} onDeleteClick={thisobj.onDeleteClick} />;
      });
    }
  }

  onClickPagelink(event) {
    const page_number = event.target.id;
    if (this.state.page_number != page_number) {
      this.setState({ page_number: page_number });
      this.props.callApi(this.state.sorting_all_fields, page_number);
    }
  }

  getPaginationLinks() {
    const { tableData, total_pages } = this.state;
    if (tableData.length > 0) {

      var rows = [], i = 0;
      while (++i <= total_pages) rows.push(i);
      return rows.map((phoneObj, index) => {
        return (
          <PaginationItem key={index}>
            <PaginationLink href="javascript:void(0)" id={index + 1} onClick={this.onClickPagelink}>
              {index + 1}
            </PaginationLink>
          </PaginationItem>
        );
      });

    }
  }

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    // this.setState({activePage: pageNumber});

    if (this.state.page_number != pageNumber) {
      this.setState({ page_number: pageNumber });
      const thisObj = this;
      setTimeout(function() {
        thisObj.props.callApi(thisObj.state.sorting_all_fields, pageNumber);
      }, 100);
    }

  }


  render() {
    // console.log("renderlisty>>>", this.state)
    const { columns, classes } = this.state;
    return (
      <div>
        <Row>

          <Col sm="12">
            <div className="add_pat_table">
              <div className="inner_pat_table">
                {
                  columns ? (
                    <Table striped>
                      <thead><tr>
                        {this.renderColumns()}
                        {this.renderActionColumn()}
                      </tr></thead>
                      <tbody>
                        { this.renderList() }
                      </tbody>
                    </Table>
                  ) : "No Patients found"
                }
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col sm="12">
            <div className="pagination">
              {/* <Pagination>
                    {/*<PaginationItem>
                        <PaginationLink previous href="#" />
                    </PaginationItem>*/}

              {/* {this.getPaginationLinks()} */}

              {/* <PaginationItem>
                        <PaginationLink next href="#" />
                    </PaginationItem>*/}
              {/* </Pagination> */} 
              <Pagination
                activePage={this.state.page_number}
                itemsCountPerPage={5}
                totalItemsCount={this.state.total_records}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange}
              />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const TableRow = ({ labels, rowIndex, objects, onEditClick, onDeleteClick }) => {
  return (
    <tr className="puneetbh">
      {
        labels.map(function(n, labelIndex) {
          return ([
            <td key={labelIndex}>{objects[rowIndex][labels[labelIndex]]}</td>,
          ]);
        })
      }

      <td>
        <a href="javascript:void(0)" onClick={onEditClick}>
          <i className="fa fa-pencil" id={objects[rowIndex].id} aria-hidden="true"></i>
        </a>
        <a href="javascript:void(0)" style={{ marginLeft: 10 }} onClick={onDeleteClick}>
          <i className="fa fa-trash-o" id={objects[rowIndex].id} aria-hidden="true"></i>
        </a>
      </td>
    </tr>
  );
};

export default RenderList;

import React, { Component } from "react";
import classes from "../../../../layouts/CoreLayout/CoreLayout.css";

class Loader extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <img className="loading_img" src={require("../../../../assets/images/loading.gif")} />
      </div>
    );
  }
}
export default (Loader);

import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Card, CardText, CardTitle,Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Form, FormGroup, Input, Nav, NavItem, NavLink,Row, TabContent, TabPane } from "reactstrap";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { fetchMasterTablesData, getPatientAllInfo } from "./../../actions";
import { calculateAge, formatDate, genderData, searchFromObj, setLocalStorage } from "utils";

class HeaderProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      patient_info: {},
      address_info: { homeAddress1: null },
      master_data: { masterState: [] },
      patient_global_data: {},
      primary_address: [{}],
      phoneNumbers: [],
    };

  }


  componentWillMount() {
    this.props.fetchMasterTablesData({ "masterdata": "masterState" });
    this.props.getPatientAllInfo(this.props.patient_id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.patient_global_data && this.props.patient_global_data !== nextProps.patient_global_data) {
      this.setState({
        primary_address: nextProps.patient_global_data.primary_address,
        phoneNumbers: nextProps.patient_global_data.phone_numbers,
        patient_info: nextProps.patient_global_data.patient_profile,
        imagePreviewUrl: nextProps.patient_global_data.patient_profile.hasOwnProperty("PhotoThumbnailPath") != null ? nextProps.patient_global_data.patient_profile.PhotoThumbnailPath : null,
      });
    }

    if (nextProps.master_data && this.props.master_data !== nextProps.master_data) {
      this.setState({
        master_data: nextProps.master_data,
      });
    }
  }

  render() {

    const { classes } = this.props;
    const { patient_info, master_data, primary_address, phoneNumbers } = this.state;

    return (
      <div>

        <Row>
          <Col sm="2">
            <div className="profile_left">
              {
                this.state.imagePreviewUrl
                  ? (<img src={this.state.imagePreviewUrl} width="100" />)
                  : (<img src={require("assets/images/no_image.png")} width="100" />)
              }
            </div>
          </Col>
          <Col sm="10">
            <div className="profile_right">
              <Col sm="12">
                <p>
                  <span className="patient_name">
                    {this.state.patient_info.FirstName} {patient_info.LastName}{" "}
                  </span>
                            ({genderData[patient_info.Gender_fk]} | {calculateAge(patient_info.DOB)}  Old | {formatDate(patient_info.DOB)})
                </p>
              </Col>
              <Col sm="4">
                <p className="profile_p">Phone:<span>
                  {
                    phoneNumbers.length > 0 ? phoneNumbers[0].PhoneNumber : "-"
                  }</span>
                </p>
              </Col>
              <Col sm="4">
                <p className="profile_p">Email:<span>{patient_info.Email}</span></p>
              </Col>
              <Col sm="4">
                <p className="profile_p">Date Added:<span>{formatDate(patient_info.createdDate)}</span></p>
              </Col>
              <Col sm="4">
                <p className="profile_p">Address:<span>
                  {
                    primary_address.length > 0 ?
                      <span>
                        {primary_address[0].HomeAddress1} {primary_address[0].HomeCity}, {searchFromObj("id", primary_address[0].HomeState_fk, master_data.masterState)}, {primary_address[0].HomeZip}
                      </span>

                      : "-"
                  }
                </span></p>
              </Col>
              <Col sm="6">
                <p className="profile_p">Last Appointment:<span>-</span></p>
              </Col>
            </div>
          </Col>
        </Row>

        <Row>
          <Col sm="12">
            <div className="profile_btm">
              <Col sm="5">
                <p>Primary Provider: {patient_info.ProviderName ? patient_info.ProviderName : "-"}</p>
              </Col>
              <Col sm="7">
                <Button color="success" className="cancel_btn">Has Allergies</Button>
                <Button color="success" className="cancel_btn">Meds Precribed</Button>
                <Button color="success" className="cancel_btn">Sexually Abused</Button>
              </Col>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  loading: state.patient.loading,
  patient_data: state.patient.patient_data,
  address_data: state.patient.address_data,
  master_data: state.patient.master_data,
  patient_all_info: state.patient.patient_all_info,
  patient_global_data: state.patient.patient_global_data,
});

export default connect(mapStateToProps, { getPatientAllInfo, fetchMasterTablesData })(HeaderProfile);

import { calculateAge, formatDate, genderData, searchFromObj, setLocalStorage } from "../../../../utils/index";
import { Col, Row } from "reactstrap";
import React, { Component } from "react";
import classes from "../../../../layouts/CoreLayout/CoreLayout.css";

class PatientInfo extends Component {
  constructor(props) {
    super(props);
    console.log("props>>>>", props);
    this.state = {
      "patient_info": props.data,
    };
  }

  componentWillReceiveProps(nextProps, props) {
    if (nextProps && nextProps.data && nextProps.data !== props.data) {
      let data = nextProps.data;
      console.log("I am here", data);
      this.setState({
        patient_info: data,
        imagePreviewUrl: (data.photoThumbnailPath) ? data.photoThumbnailPath : null,
      });
    }
  }
  render() {
    if (!this.state.patient_info) {
      return (
        <div>
          <div>
            <h2>Patient Profile ...</h2>
          </div>
        </div>
      );
    }
    // const { patient_info, address_info, master_data } = this.state;
    return (
      <div>
        <div>
          <h2>Patient Profile</h2>
        </div>
        <Row>
          <Col sm="2">
            <div className={classes.profile_left}>
              {this.state.imagePreviewUrl ? (<img src={this.state.imagePreviewUrl} width="100" />) : ""}
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
export default (PatientInfo);

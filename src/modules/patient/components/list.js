import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { change, Field, reduxForm, reset } from "redux-form";
import { Button, Card, CardText, CardTitle, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Form, FormGroup, Input, Label, Nav, NavItem,NavLink, Pagination3, PaginationItem,PaginationLink ,Row , TabContent, Table, TabPane } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { maskInputField, RadioButton, renderDateField, renderField, renderSelectField, renderUploadField } from "app/reduxForm/components/";
import { email, maxLength15, required, requiredDropdown, tooOld } from "app/reduxForm/validations";
import { fetchMasterTablesData, getAllPatients } from "./../actions";
import { calculateAge, formatDate } from "utils";
import { browserHistory, Link } from "react-router";
import { AdvanceSearch, SimpleSearch } from "./search_components";
import Pagination from "./pagination/Pagination";


const validate = (values) => {
  const errors = {};

//   if(!values.dob) {
//     errors.dob = 'Please enter DOB.'
//   }

//   if(values.title=='0') {
//     errors.title = 'Please enter DOB.'
//   }
//   console.log(errors);
//   return errors
};


class PatientList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      all_patients: [],
      master_data: { masterMaritalStatus: [], masterPatientStatus: [], provider: [], masterReferral: [] },
      total_pages: 0,
      current_page: 1,
      total_records: 1,
      page_number: 1,
      sorting_all_fields: null,
      show_advance_search: false,
      genderState: "Male",
      created_from_value: "",
      created_to_value: "",
      dob_from_value: "",
      dob_to_formatted: null,
      filters: {},
      includes: "include=provider,mastergender,patientinsurancedetails",
      page_size: "5",
      numeric_sorting: "fa-sort-numeric-asc",
      char_sorting: "fa-sort-alpha-asc",
      created_from_formatted: null,
      created_to_formatted: null,
      dob_from_formatted: null,
      loading: false,
      activePage: 1,
    };
    this.handlePageChange = this.handlePageChange.bind(this);
    this.onClickPagelink = this.onClickPagelink.bind(this);
    this.handleSorting = this.handleSorting.bind(this);
    this.onChangeGender = this.onChangeGender.bind(this);
    this.handleCreatedFromDateChange = this.handleCreatedFromDateChange.bind(this);
    this.handleCreatedToDateChange = this.handleCreatedToDateChange.bind(this);
    this.handleDobFromDateChange = this.handleDobFromDateChange.bind(this);
    this.handleDobToDateChange = this.handleDobToDateChange.bind(this);
    this.onAdvanceButtonClick = this.onAdvanceButtonClick.bind(this);
    // this.onHideAdvance = this.onHideAdvance.bind(this)

    localStorage.removeItem("address_id");
    localStorage.removeItem("insurance_id");
    localStorage.removeItem("patientID_fk");
    localStorage.removeItem("preference_id");
    document.title = "Patient Search";
  }

  componentWillMount() {
    this.fetchpatients();
  }

  componentDidMount() {
    this.props.fetchMasterTablesData();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      loading: nextProps.loading,
      all_patients: nextProps.all_patients,
    });
    // console.log("nextProps==", nextProps)
    if (nextProps.all_patients.meta && this.props.all_patients !== nextProps.all_patients) {
      this.setState({
        total_pages: nextProps.all_patients.meta["total-pages"],
        current_page: nextProps.all_patients.meta["current-page"],
        total_records: nextProps.all_patients.meta["total-records"],
      });
    }
    if (nextProps.master_data && this.props.master_data !== nextProps.master_data) {
      this.setState({
        master_data: nextProps.master_data,
      });
    }
  }

  onSubmitubmitHandler(formProps) {
    const filterObj = {};
    if (formProps.gender_fk && formProps.gender_fk != "") {
      filterObj.gender = `eq:${formProps.gender_fk}`;
    }
    if (formProps.firstName && formProps.firstName != "") {
      filterObj.firstName = `like:${formProps.firstName}`;
    }
    if (formProps.middleName && formProps.middleName != "") {
      filterObj.middleName = `like:${formProps.middleName}`;
    }
    if (formProps.lastName && formProps.lastName != "") {
      filterObj.lastName = `like:${formProps.lastName}`;
    }
    if (formProps.ssn && formProps.ssn != "") {
      filterObj.ssn = `eq:${formProps.ssn}`;
    }
    if (formProps.clientStatus_fk && formProps.clientStatus_fk != "") {
      filterObj.clientStatus_fk = `eq:${formProps.clientStatus_fk}`;
    }
    if (formProps.patient_id && formProps.patient_id != "") {
      filterObj.id = `eq:${formProps.patient_id}`;
    }
    if (formProps.keyword && formProps.keyword != "") {
      filterObj.searchkey = `like:${formProps.keyword}`;
    }
    if (this.state.created_from_formatted != null) {
      filterObj.fromDate = `ge:${this.state.created_from_formatted}`;
    }
    if (this.state.created_to_formatted != null) {
      filterObj.toDate = `le:${this.state.created_to_formatted}`;
    }
    if (this.state.dob_from_formatted != null) {
      filterObj.fromDOB = `ge:${this.state.dob_from_formatted}`;
    }
    if (this.state.dob_to_formatted != null) {
      filterObj.toDOB = `le:${this.state.dob_to_formatted}`;
    }
    this.setState({
      filters: filterObj,
      page_number: 1,
      sort_fields: null,
    });


    let thisObj = this;
    setTimeout(function() {
      thisObj.fetchpatients();
    }, 100);
  }

  extractDataFromObject(matchValue, dataArray) {
    let returnValue = "NA";
    return dataArray.map((typeObj, index) => {
      if (typeObj.id === matchValue) {
        return typeObj.value;
      }
    });
  }

  renderList() {
    // console.log("new state", this.state)
    const patients = this.state.all_patients.data ? this.state.all_patients.data : {};
    const { provider, masterPatientStatus, masterMaritalStatus } = this.state.master_data;
    if (patients.length > 0) {
      return patients.map((patient, index) => {
        return (
          <tr key={index}>
            <td>{patient.attributes.ProviderName ? patient.attributes.ProviderName : "-"}</td>
            <td>{this.extractDataFromObject(patient.attributes.ClientStatus_fk, masterPatientStatus)}</td>
            <td>{patient.attributes.FirstName}</td>
            <td>{patient.attributes.LastName}</td>
            <td>{patient.attributes.Gender}</td>
            <td>{formatDate(patient.attributes.DOB)}</td>
            <td>{patient.attributes.PatientInsurance ? patient.attributes.PatientInsurance : "-"}</td>
            <td>-</td>
            <td>
              <Link to={`/patient/profile/${patient.attributes.PatientID}`}>
                <i className="fa fa-edit" aria-hidden="true"></i>
              </Link>
            </td>
          </tr>
        );
      });
    }
  }

  getPaginationLinks() {
    const patients = this.state.all_patients.data ? this.state.all_patients.data : {};
    const { total_records, total_pages, page_number } = this.state;
    if (patients.length > 0) {

      var page_numbers_display = 4;var rows = [], i = 0;
      if (page_number != 1) {
        page_numbers_display = page_numbers_display + parseInt(page_number);
        i = parseInt(page_number) ;
        if (page_numbers_display > total_pages) {
          page_numbers_display = parseInt(page_number) + (parseInt(total_pages) - parseInt(page_number));
          i = page_number - 4 < 0 ? 0 : page_number - 4;
        }

      }

      console.log("i=", i, "    page_numbers_display=", page_numbers_display, "  rows=", rows);
      while (++i <= page_numbers_display) rows.push(i);

      return rows.map((phoneObj, index) => {
        return (
          <PaginationItem key={index}>
            <PaginationLink href="javascript:void(0)" id={phoneObj} onClick={this.onClickPagelink}>
              {phoneObj}
            </PaginationLink>
          </PaginationItem>
        );
      });

    }
  }

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    // this.setState({activePage: pageNumber});

    if (this.state.page_number != pageNumber) {
      this.setState({ page_number: pageNumber });
      const thisObj = this;
      setTimeout(function() {
        thisObj.fetchpatients();
      }, 100);
    }

  }

  onClickPagelink(event) {
    const page_number = event.target.id;
    if (this.state.page_number != page_number) {
      this.setState({ page_number: page_number });
      const thisObj = this;
      setTimeout(function() {
        thisObj.fetchpatients();
      }, 100);
    }
  }

  handleSorting(event) {
    // ?sort=ssn,-id&page[number]=2
    const sort_field = event.target.id;


    if (sort_field == "id" || sort_field == "ssn" || sort_field == "dob") {
      this.setState({
        numeric_sorting: this.state.numeric_sorting == "fa-sort-numeric-asc" ? "fa-sort-numeric-desc" : "fa-sort-numeric-asc",
      });
    } else {
      this.setState({
        char_sorting: this.state.char_sorting == "fa-sort-alpha-asc" ? "fa-sort-alpha-desc" : "fa-sort-alpha-asc",
      });
    }

    var foundPositive = this.state.sorting_all_fields === sort_field;
    var foundNegative = this.state.sorting_all_fields === `-${sort_field}`;

    if (foundPositive) {
      this.state.sorting_all_fields = `-${sort_field}`;
    } else if (foundNegative) {
      this.state.sorting_all_fields = sort_field;
    } else {
      this.state.sorting_all_fields = `-${sort_field}`;
    }

    this.fetchpatients();
  }

  fetchpatients() {
    const searchObj = {
      page_number: this.state.page_number,
      sort_fields: this.state.sorting_all_fields,
      filters: this.state.filters,
      includes: this.state.includes,
      page_size: this.state.page_size,
    };
    this.props.getAllPatients(searchObj);
  }

  onChangeGender(event) {
    this.setState({ genderState: event.target.value });
  }
  handleCreatedFromDateChange(date) {
    this.setState({
      created_from_value: date,
      created_from_formatted: moment(date).format("YYYY-MM-DD"),
    });
  }

  handleCreatedToDateChange(date) {
    this.setState({
      created_to_value: date,
      created_to_formatted: moment(date).format("YYYY-MM-DD"),
    });
  }

  handleDobFromDateChange(date) {
    // console.log(date)
    this.setState({
      dob_from_value: date,
      dob_from_formatted: moment(date).format("YYYY-MM-DD"),
    });
  }

  handleDobToDateChange(date) {
    this.setState({
      dob_to_value: date,
      dob_to_formatted: moment(date).format("YYYY-MM-DD"),
    });
  }

  onAdvanceButtonClick() {
    this.setState({
      show_advance_search: this.state.show_advance_search == true ? false : true,
    });
    this.props.dispatch(change("PatientList", "keyword", ""));
  }


  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props;
    const { classes } = this.props;
    const { genderState, created_from_value, created_to_value, dob_from_value, dob_to_value } = this.state;

    return (
      <div className="wpad_1 classes.fl_left_back">
        <div className="patient_dv1">
          <h2 className="patient_dv">Patient Listing</h2>
        </div>
        <form onSubmit={handleSubmit(this.onSubmitubmitHandler.bind(this))}>

          <div className="outer_listing">

            {
              !this.state.show_advance_search ?
                <SimpleSearch classes={classes} onClickBtn={this.onAdvanceButtonClick} /> : null
            }


            {
              this.state.show_advance_search ?
                <AdvanceSearch {...this.props} classes={classes} masterPatientStatus=
                  {this.state.master_data.masterPatientStatus} genderState={this.state.genderState} onChangeGender={this.onChangeGender} handleCreatedFromDateChange={this.handleCreatedFromDateChange} created_from_value={created_from_value} created_to_value={created_to_value} handleCreatedToDateChange={this.handleCreatedToDateChange} dob_from_value={dob_from_value} dob_to_value={dob_to_value} handleDobFromDateChange={this.handleDobFromDateChange} handleDobToDateChange={this.handleDobToDateChange} onHideAdvance={this.onAdvanceButtonClick}
                /> : null
            }


            <Row>
              <Col sm="12">
                <div className="add_pat">
                  <Button
                    onClick={() => browserHistory.push("/patient")}
                    color="success"
                  >ADD PATIENT</Button>
                </div>
              </Col>
            </Row>

            <Row>
              <Col sm="12">
                <div className="add_pat_table">
                  <div className="table-responsive">
                    {
                      this.state.all_patients.data && this.state.all_patients.data.length > 0 ? (
                        <Table striped>
                          <thead>
                            <tr>
                              <th className="crsr_pointer" id="ProviderName" onClick={this.handleSorting}>
                                                        Provider
                                <i id="FirstName" className={`fa ${classes.lpad} ${this.state.char_sorting}`} aria-hidden="true"></i>
                              </th>
                              <th className="crsr_pointer" id="ClientStatus_fk" onClick={this.handleSorting}>
                                                        Status
                                <i className={`fa ${classes.lpad} ${this.state.numeric_sorting}`} aria-hidden="true"></i>
                              </th>
                              <th className="crsr_pointer" id="FirstName" onClick={this.handleSorting}>
                                                        First
                                <i id="FirstName" className={`fa ${classes.lpad} ${this.state.char_sorting}`} aria-hidden="true"></i>
                              </th>
                              <th className="crsr_pointer" id="LastName" onClick={this.handleSorting}>
                                                        Last
                                <i id="LastName" className={`fa ${classes.lpad} ${this.state.char_sorting}`} aria-hidden="true"></i>
                              </th>
                              <th className="crsr_pointer" id="Gender" onClick={this.handleSorting}>
                                                        Gender
                                <i className={`fa ${classes.lpad} ${this.state.char_sorting}`} aria-hidden="true"></i>
                              </th>
                              <th className="crsr_pointer" id="Gender" onClick={this.handleSorting}>
                                                        DOB
                                <i className={`fa ${classes.lpad} ${this.state.numeric_sorting}`} aria-hidden="DOB"></i>
                              </th>
                              <th className="crsr_pointer" id="PatientInsurance" onClick={this.handleSorting}>
                                                        Pri Insurance
                                <i id="LastName" className={`fa ${classes.lpad} ${this.state.char_sorting}`} aria-hidden="true"></i>
                              </th>
                              <th>Next Appointment</th>
                              <th>Actions</th>
                            </tr>
                          </thead>
                          <tbody>
                            { this.renderList() }
                          </tbody>
                        </Table>
                      ) : "No Patients found"
                    }

                  </div>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12" className="text-center">
                <div className="pagination">
                  {/* <Pagination> */}
                  {/* <PaginationItem>
                                            <PaginationLink previous href="#" />
                                        </PaginationItem>*/}

                  {/* {this.getPaginationLinks()} */}

                  {/* <PaginationItem>
                                            <PaginationLink next href="#" />
                                        </PaginationItem>*/}
                  {/* </Pagination> */}
                  <Pagination
                    activePage={this.state.page_number}
                    itemsCountPerPage={5}
                    totalItemsCount={this.state.total_records}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange}
                  />
                </div>
              </Col>
            </Row>

          </div>
        </form>
        {this.state.loading ? <img className="loading_img" src={require("./../../../assets/images/loading.gif")} /> : ""}

      </div>

    );
  }
}

const mapStateToProps = (state) => ({
  loading: state.patient.loading,
  all_patients: state.patient.all_patients,
  master_data: state.patient.master_data,
});

PatientList = reduxForm({
  form: "PatientList", // a unique identifier for this form
  // asyncValidate,
  // asyncBlurFields: [ 'email' ]
})(PatientList);

export default connect(mapStateToProps, { fetchMasterTablesData, getAllPatients })(PatientList);

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { change, Field, formValueSelector, initialize, reduxForm, reset } from "redux-form";
import { Button, Card, CardText, CardTitle, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Form, FormGroup, Input, Label, Nav, NavItem, NavLink, Row, TabContent, TabPane } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { maskInputField, RadioButton, renderDateField, renderField, renderFieldWithoutValue, renderSelectField, renderUploadField } from "app/reduxForm/components/";
import { email, maxLength, maxLength15, number, onlyLetters, required, requiredDropdown, tooOld } from "app/reduxForm/validations";
import { clearForm, fetchMasterTablesData, getAddressData, getStateByCountry, mailingAddressAsPrimary, saveUpdatePatientAddress } from "./../actions";
import Alert from "react-s-alert";
import { handleFormErrors } from "utils";
import { browserHistory, Link } from "react-router";

const validate = (values) => {
  const errors = {};


  if (values.title == "0") {
    errors.title = "Please enter DOB.";
  }
  return errors;
};


class PatientAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      master_data: { masterCountry: [], masterState: [], masterPhoneType: [] },
      phoneList: [],
      mailingAddress1: "",
      mailingCity: "",
      mailingZip: "", mailingState_fk: "",
      mailCountry_fk: "",
      mailingData: null,
      sameMailingAddress: false,
      patientID_fk: localStorage.getItem("patientID_fk"),
      address_updated: false,
      address_id: localStorage.getItem("address_id") ? localStorage.getItem("address_id") : null,
      address_data: {},
      patient_global_data: {},
      phone_removed: false,
      phone_error: false,
      state_list: [],
    };
    this.onCheckSameAddress = this.onCheckSameAddress.bind(this);
    this.onChangeEvent = this.onChangeEvent.bind(this);
    this.handleNewRowSubmit = this.handleNewRowSubmit.bind(this);
  }

  componentWillMount() {
    this.props.clearForm();
    if (this.state.address_id != null) {
      this.props.getAddressData(this.state.address_id, this.state.patientID_fk);
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  componentWillReceiveProps(nextProps) {
    console.log("next props>>>", nextProps);
    if (nextProps.master_data != undefined) {
      this.setState({
        master_data: nextProps.master_data,
        loading: false,
      });
    }

    if (nextProps.patient_global_data.primary_address
            && nextProps.patient_global_data.primary_address.length > 0
            && this.state.sameMailingAddress != nextProps.patient_global_data.primary_address[0].IsMailingSame
    ) {
      this.setState({
        // sameMailingAddress: nextProps.patient_global_data.primary_address[0].IsMailingSame
      });
    }
    if (nextProps.state_list && nextProps.state_list !== this.props.state_list) {
      this.setState({
        state_list: nextProps.state_list,
      });
    }

    if (nextProps.address_updated == true) {
      this.props.onSubmitSuccess("3");
    }

    if (nextProps.patient_global_data != null && nextProps.patient_global_data !== this.state.patient_global_data
            && this.state.phone_removed === false && nextProps.patient_global_data.phone_numbers.length > 0
            && this.state.phoneList.length == 0
    ) {
      let phoneListArr = [];
      for (var i = 0; i < nextProps.patient_global_data.phone_numbers.length; i++) {
        phoneListArr.push({
          phone_type: nextProps.patient_global_data.phone_numbers[i].PhoneNumberTypeId_fk,
          phone_number: nextProps.patient_global_data.phone_numbers[i].PhoneNumber,
          phoneNumber: nextProps.patient_global_data.phone_numbers[i].PhoneNumber,
          phoneNumberTypeId_fk: nextProps.patient_global_data.phone_numbers[i].PhoneNumberTypeId_fk,
          patientID_fk: nextProps.patient_global_data.phone_numbers[i].PatientID_fk,
        });
      }
      if (phoneListArr != null) {
        this.setState({ phoneList: phoneListArr, phone_number: null });
      }
    }

    if (this.state.sameMailingAddress) {
      if (nextProps.homeAddress1 !== this.props.homeAddress1) {
        this.props.dispatch(change("PatientAddress", "mailingAddress1", nextProps.homeAddress1));
      }
      if (nextProps.homeCity !== this.props.homeCity) {
        this.props.dispatch(change("PatientAddress", "mailingCity", nextProps.homeCity));
      }
      if (nextProps.homeZip !== this.props.homeZip) {
        this.props.dispatch(change("PatientAddress", "mailingZip", nextProps.homeZip));
      }
      if (nextProps.homeCountry_fk !== this.props.homeCountry_fk) {
        this.props.dispatch(change("PatientAddress", "mailCountry_fk", nextProps.homeCountry_fk));
      }
      if (nextProps.homeState_fk !== this.props.homeState_fk) {
        this.props.dispatch(change("PatientAddress", "mailingState_fk", nextProps.homeState_fk));
      }
    }

    if (this.state.sameMailingAddress) {
      if (nextProps.homeAddress1 !== this.props.homeAddress1) {
        this.props.dispatch(change("PatientAddress", "mailingAddress1", nextProps.homeAddress1));
      }
      if (nextProps.homeCity !== this.props.homeCity) {
        this.props.dispatch(change("PatientAddress", "mailingCity", nextProps.homeCity));
      }
      if (nextProps.homeZip !== this.props.homeZip) {
        this.props.dispatch(change("PatientAddress", "mailingZip", nextProps.homeZip));
      }
      if (nextProps.homeCountry_fk !== this.props.homeCountry_fk) {
        this.props.dispatch(change("PatientAddress", "mailCountry_fk", nextProps.homeCountry_fk));
      }
      if (nextProps.homeState_fk !== this.props.homeState_fk) {
        this.props.dispatch(change("PatientAddress", "mailingState_fk", nextProps.homeState_fk));
      }
    }
  }

  onChangeEvent(e) {
    this.setState({ [e.target.name]: e.target.value });
    if (this.state.sameMailingAddress == true) {
      // this.onCheckSameAddress()
    }
  }

  onSubmitubmitHandler(formProps) {
    formProps.patientID_fk = this.state.patientID_fk;
    formProps.id = this.state.address_id;
    formProps.phoneData = { patientID: this.state.patientID_fk, phoneNumbers: this.state.phoneList };
    formProps.CreatedBy_fk = 1;
    formProps.isMailingSame = this.state.sameMailingAddress;
    this.props.saveUpdatePatientAddress(formProps);
    // console.log("company list: ", this.state);
    // console.log("complete form data: ", formProps);
  }

  onCheckSameAddress(e) {
    let sameMailingAddress = false;
    // console.log("manish==", this.state.sameMailingAddress);
    if (this.state.sameMailingAddress === false) {
      let homeAddress = {
        homeAddress1: this.props.homeAddress1,
        homeCity: this.props.homeCity,
        homeZip: this.props.homeZip,
        homeCountry_fk: this.props.homeCountry_fk,
        homeState_fk: this.props.homeState_fk,
        mailingAddress1: this.props.homeAddress1,
        mailingCity: this.props.homeCity,
        mailingZip: this.props.homeZip,
        mailCountry_fk: this.props.homeCountry_fk,
        mailingState_fk: this.props.homeState_fk,
      };
      this.setState({ sameMailingAddress: true });
      this.props.mailingAddressAsPrimary(homeAddress);
    } else if (this.state.sameMailingAddress == true) {
      this.setState({ sameMailingAddress: false });
    }
  }

  handleNewRowSubmit() {
    if (this.props.phone_type == null || this.props.phone_number == null) {
      this.setState({ phone_error: "PLease select Phone type and enter Phone number." });
      return false;
    } else if (this.props.phone_number.match(/\d/g).length < 10) {
      this.setState({ phone_error: "PLease enter valid Phone number." });
      return false;
    }

    this.setState({ phone_error: false });

    const phoneListObj = {
      phone_type: this.props.phone_type,
      phone_number: this.props.phone_number,
      phoneNumber: this.props.phone_number,
      phoneNumberTypeId_fk: this.props.phone_type,
      patientID_fk: this.state.patientID_fk,
    };
    this.props.dispatch(change("PatientAddress", "phone_number", ""));
    this.props.dispatch(change("PatientAddress", "phone_type", ""));
    this.setState({ phoneList: this.state.phoneList.concat([phoneListObj]), phone_number: null });
  }

    handleRowRemove = (event, company) => {
      var index = -1;
      var clength = this.state.phoneList.length;
      for (var i = 0; i < clength; i++) {
        if (this.state.phoneList[i].phone_number === company.phone_number
                && this.state.phoneList[i].phoneNumberTypeId_fk === company.phone_type
        ) {
          index = i;
          break;
        }
      }
      this.state.phoneList.splice(index, 1);
      this.setState({ phoneList: this.state.phoneList, phone_removed: true });
    };

    renderPhoneList() {

      if (this.state.phoneList.length > 0) {
        return this.state.phoneList.map((phoneObj, index) => {
          return (
            <Row key={index}>
              <Col sm="12">
                <div className="first_tab sec_tab">
                  <Col sm="2">
                    <div className="phone1">
                      <h2>{this.getPhoneType(phoneObj.phone_type)}</h2>
                    </div>
                  </Col>
                  <Col sm="2">
                    <div className="phone1">
                      <h3>{phoneObj.phone_number}</h3>
                    </div>
                  </Col>
                  <Col sm="2">
                    <div className="add_option  add_option1">
                      <a href="javascript:void(0)"
                        // onClick={this.handleRowRemove}
                        onClick={(e) => this.handleRowRemove(e, phoneObj)}
                      >
                        <i className="fa fa-minus-circle" aria-hidden="true"></i>Remove
                      </a>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
          );
        });
      }
    }

    getPhoneType(phone_type) {
      return this.state.master_data.masterPhoneType.map((typeObj, index) => {
        if (typeObj.id == phone_type) {
          return typeObj.value;
        }
      });
    }


    render() {
      // console.log("new state" , this.state)
      const { handleSubmit, pristine, reset, submitting } = this.props;
      const { classes } = this.props;
      const { state_list } = this.state;
      const { masterCountry, masterPhoneType, masterState } = this.state.master_data;
      const dropdownData = [{
        "id": "18",
        "value": "Mr",
      }, {
        "id": "17",
        "value": "Ms",
      }];
      return (
        <div className="hello">
          <form onSubmit={handleSubmit(this.onSubmitubmitHandler.bind(this))}>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <div className="patient_dv">
                    <h2 className="sub_head">Primary Address</h2>
                  </div>
                  <Col sm="8">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="homeAddress1" type="text" component={renderField}
                          label="Address" plaeholder="Address 1"
                          classes={classes}
                          fieldValue={this.state.homeAddress1}
                          validate={[required("Address")]}
                          onChangeEvent={this.onChangeEvent}
                          maxlength="150"
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="homeCity" type="text" component={renderField}
                          label="City" plaeholder="e.g Los Angles"
                          classes={classes}
                          validate={[required("City")]}
                          onChangeEvent={this.onChangeEvent}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="homeCountry_fk" emptyValue="Select Country"
                          templateList={masterCountry}
                          component={renderSelectField}
                          validate={[required("Country")]}
                          label="Country"
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="homeState_fk" emptyValue="Select State"
                          templateList={masterState}
                          // selectedValue="2"
                          component={renderSelectField}
                          validate={[required("State")]}
                          label="State"
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="homeZip" type="text" component={renderField}
                          label="Zip Code" plaeholder="e.g 011100"
                          validate={[required("Zip Code"), number, maxLength(5)]}
                          classes={classes}
                          onChangeEvent={this.onChangeEvent}
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>

            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <div className="patient_dv">
                    <h2 className="sub_head1">Mailing Address</h2>
                    <Label check className="patient_check">
                      <Input type="checkbox"
                        checked={this.state.sameMailingAddress ? true : false}
                        onClick={this.onCheckSameAddress} />{" "}
                                        Same as Primary Address
                    </Label>
                  </div>
                  <Col sm="8">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="mailingAddress1"
                          // fieldValue={this.state.mailingAddress1} 
                          type="text"
                          component={renderField}
                          label="Address" plaeholder="Address 1"
                          classes={classes}
                          // onChangeEvent={this.onChangeEvent}
                          maxlength="150"
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="mailingCity" type="text" component={renderField}
                          label="City" plaeholder="e.g Los Angles"
                          classes={classes}
                          // fieldValue={this.state.mailingCity} 
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="mailCountry_fk" emptyValue="Select Country"
                          templateList={masterCountry}
                          component={renderSelectField}
                          selectedValue={this.state.mailCountry_fk}
                          label="Country"
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="mailingState_fk" emptyValue="Select State"
                          templateList={masterState}
                          component={renderSelectField}
                          label="State"
                          classes={classes}
                          selectedValue={this.state.mailingState_fk}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="mailingZip" type="text" component={renderField}
                          label="Zip Code" plaeholder="e.g 011100"
                          classes={classes}
                          validate={[number, maxLength(5)]}
                          // fieldValue={this.state.mailingZip} 
                        />
                      </div>
                    </div>
                  </Col>
                </div>
              </Col>
            </Row>

            <Row>
              <Col sm="12">
                <div className="first_tab">
                  <div className="patient_dv">
                    <h2 className="sub_head">Phone Number</h2>
                  </div>
                  <Col sm="2">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <Field name="phone_type" emptyValue="Select Phone"
                          templateList={masterPhoneType}
                          component={renderSelectField}
                          label="Phone"
                          classes={classes}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="txtbx1">
                      <div className="col-sm">
                        <div className="phn_text">
                          <Field name="phone_number" type="text" component={maskInputField}
                            plaeholder="Phone"
                            maskFormat="(999)-999-9999"
                            classes={classes}
                            onChangeEvent={this.onChangeEvent}
                            fieldValue={this.state.phone_number}
                          />
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col sm="4">
                    <div className="add_option+ ' '+classes.res_add_option ">
                      <a href="javascript:void(0)" name="add_phone_button" onClick={this.handleNewRowSubmit}> <i className="fa fa-plus-circle" aria-hidden="true"></i>Add Phone</a>
                    </div>
                  </Col>
                </div>
              </Col>
              {
                this.state.phone_error ?
                  (
                    <Col sm="12">
                      <div className="redColor">
                        <Col sm="12">
                          {this.state.phone_error}
                        </Col>
                      </div>
                    </Col>
                  )
                  : ""
              }
            </Row>

            {this.state.phoneList ? this.renderPhoneList() : null}

            <div className="bottom_dv">
              <div className="bottom_dv_inner">

                <Button color="success" type="submit" className="patient_btn">
                  {this.state.address_id ? "UPDATE ADDRESS" : "ADD ADDRESS DETAILS"}
                </Button>
                <Button color="success" onClick={() => browserHistory.push("/patient/list")} className="cancel_btn">CANCEL</Button>
              </div>
            </div>
          </form>
          {this.props.loading ? <img className="loading_img" src={require("./../../../assets/images/loading.gif")} /> : ""}
        </div>

      );
    }
}

const selector = formValueSelector("PatientAddress"); // <-- same as form name

const mapStateToProps = (state) => ({
  master_data: state.patient.master_data,
  homeCountry_fk: selector(state, "homeCountry_fk"),
  homeState_fk: selector(state, "homeState_fk"),
  phone_type: selector(state, "phone_type"),
  phone_number: selector(state, "phone_number"),
  homeAddress1: selector(state, "homeAddress1"),
  homeCity: selector(state, "homeCity"),
  homeZip: selector(state, "homeZip"),
  mailingCity: "sdfdsfds",
  initialValues: state.patient.address_data,
  loading: state.patient.loading,
  patient_global_data: state.patient.patient_global_data,
  state_list: state.patient.state_list,
  address_updated: state.patient.address_updated,
});

PatientAddress = reduxForm({
  form: "PatientAddress", // a unique identifier for this form
  validate,
  enableReinitialize: true,
  onSubmitFail: (errors) => {
    handleFormErrors(errors);
  },
  // asyncValidate,
  // asyncBlurFields: [ 'email' ]
})(PatientAddress);

export default connect(mapStateToProps, { fetchMasterTablesData, mailingAddressAsPrimary, saveUpdatePatientAddress, getAddressData, clearForm, getStateByCountry })(PatientAddress);

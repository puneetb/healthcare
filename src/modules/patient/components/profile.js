import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Card, CardText, CardTitle,Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Form, FormGroup, Input, Nav, NavItem, NavLink,Row, TabContent, TabPane } from "reactstrap";
import TabSection from "./tabSection";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { getPatientAllInfo, getPatientData } from "./../actions";
import { calculateAge, formatDate, genderData, searchFromObj, setLocalStorage } from "utils";
import HeaderProfile from "./common/HeaderProfile";


class PatientIntake extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeTab: "1",
      startDate: moment(),
      patient_info: {},
      address_info: { homeAddress1: null },
      master_data: {},
      patient_global_data: {},
      patient_id: this.props.params.splat,
    };
    localStorage.removeItem("address_id");
    localStorage.removeItem("insurance_id");
    localStorage.removeItem("patientID_fk");
    localStorage.removeItem("preference_id");
    setLocalStorage("patientID_fk", this.props.params.splat);
    document.title = "Patient Profile";
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  componentWillMount() {
    this.props.getPatientAllInfo(this.props.params.splat);
    // this.props.getPatientData(this.props.params.splat);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.patient_data && this.props.patient_data !== nextProps.patient_data) {
      let pdata = nextProps.patient_data;
      this.setState({
        patient_info: pdata,
        imagePreviewUrl: pdata.PhotoThumbnailPath,
        loading: nextProps.loading,
      });
      if (nextProps.patient_global_data && nextProps.patient_global_data != null) {
        if (nextProps.patient_global_data.address_id != null) {
          setLocalStorage("address_id", nextProps.patient_global_data.address_id);
        }
        if (nextProps.patient_global_data.insurance_id != null) {
          setLocalStorage("insurance_id", nextProps.patient_global_data.insurance_id);
        }
      }

    }

    if (nextProps.address_data && this.props.address_data !== nextProps.address_data) {
      this.setState({
        address_info: nextProps.address_data,
      });
    }
  }

  render() {
    const { classes } = this.props;
    const { patient_info, address_info, master_data } = this.state;

    return (
      <Col xs="3" sm="11" className="wpad_1">
        <div className="right_con1">
          <div className="patient_dv">
            <h2 className="patient_dv">Patient Profile</h2>
          </div>

          <HeaderProfile classes={classes} patient_id={this.state.patient_id} />

          <div className="bottom_con">
            <div className="tab_layout">
              <TabSection classes={classes} />
            </div>
            <div className="footer">
              <p><i className="fa fa-copyright " aria-hidden="true" /> 2017 Healthcare. All Right Reserved.</p>
            </div>
          </div>
        </div>
      </Col>
    );
  }
}

const mapStateToProps = (state) => ({
  loading: state.patient.loading,
  patient_data: state.patient.patient_data,
  insurance_data: state.patient.insurance_data,
  address_data: state.patient.address_data,
  master_data: state.patient.master_data,
  patient_all_info: state.patient.patient_all_info,
  patient_global_data: state.patient.patient_global_data,
});

export default connect(mapStateToProps, { getPatientAllInfo, getPatientData })(PatientIntake);

import React, { Component } from "react";
import { Button, Card, CardText, CardTitle, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Form, FormGroup, Input, Nav, NavItem, NavLink, Row, TabContent, TabPane } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import Demographic from "./demographic";
import Address from "./address";
import Insurance from "./insurance";
import Alert from "react-s-alert";
import { getPatientData } from "./../actions";
import { connect } from "react-redux";


class TabSection extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: "1",
      patient_data: {},
    };
    this.toggle = this.toggle.bind(this);
    this.checkPatientIs = this.checkPatientIs.bind(this);
  }

  checkPatientIs(tab) {
    if (localStorage.getItem("patientID_fk") == null && tab != "1") {
      Alert.error("Please enter patient Demographic details.", {
        html: true,
        timeout: 2000,
      });
      this.setState({
        activeTab: "1",
      });
    }
  }

  toggle(tab) {
    if (this.state.activeTab !== tab && tab != "" && tab != null) {

      this.setState({
        activeTab: tab,
      });

      this.checkPatientIs(tab);
    }
  }

  renderTabContent(tab, classes) {
    // console.log("inside tab section tab number: ", tab)
    if (tab == 1) {
      return (<Demographic classes={classes} patientInfo={this.state.patient_data} onSubmitSuccess={this.toggle} />);
    }
    if (tab == 2) {
      return (<Address classes={classes} onSubmitSuccess={this.toggle} />);
    }
    if (tab == 3) {
      return (<Insurance classes={classes} />);
    }
  }


  render() {
    const { classes } = this.props;
    return (
      <div>
        <Nav tabs className="name">
          <NavItem>
            <NavLink
              className={this.state.activeTab === "1" ? "active" : null }
              onClick={() => {
                this.toggle("1");
              }}
            >
              Demographic Information
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={this.state.activeTab === "2" ? "active" : null }
              onClick={() => {
                this.toggle("2");
              }}
            >
              Address
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={this.state.activeTab === "3" ? "active" : null }
              onClick={() => {
                this.toggle("3");
              }}
            >
              Insurance
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          {this.renderTabContent(this.state.activeTab, classes)}
          {/* <TabPane tabId="1">
           <Demographic classes={classes} onSubmitSuccess={this.toggle} />
          </TabPane>
          <TabPane tabId="2">
            <Address classes={classes} onSubmitSuccess={this.toggle} />
          </TabPane>
          <TabPane tabId="3">
            <Insurance classes={classes} />
          </TabPane>*/}
        </TabContent>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    // patient_data: state.patient.patient_data
  };
};

export default connect(mapStateToProps, { getPatientData })(TabSection);

import { createAction } from "redux-actions";
import axios from "axios";
// import { checkHttpStatus, parseJSON } from 'utils';
import {
  CHECK_PATIENT_EXIST, CREATE_PATIENT_API, FAMILY_HISTORY, FETCH_ALL_PATIENTS, FETCH_PATIENT, FETCH_PATIENT_ADDRESS, FETCH_PATIENT_ALL_INFO, FETCH_PATIENT_INSURANCE, FETCH_PATIENT_VITALS, GET_STATE_BY_COUNTRY, patientActionTypes, SAVE_PHONE_NUMBER, SAVE_PREFRENCES, SOCIAL_HISTORY, UPDATE_FAMILY_HISTORY, UPDATE_PATIENT_ADDRESS, UPDATE_PATIENT_INSURANCE_DETAILS, UPDATE_PREFERENCES } from "./constants";

import { AXIOS_INSTANCE,
  checkHttpStatus,
  createFetchObj,
  createRequestActionTypes,
  errorHandler,
  fetchApiHeaders, GET_MASTER_TABLES_DATA,
  getFailure,
  getRequest, getSuccess, jsonApiHeader,
  parseJSON, setLocalStorage,
} from "app/globalConstants";
import Alert from "react-s-alert";
import { apiInputFormatDate, createSearchUrl, extractDataFromObject, mergeObjects, toCamel } from "utils";


export const fetchMasterTablesData = (fieldObj) => {

  console.log("here in fetchMasterTablesData");
  if (!fieldObj) {
    var fieldObj = { "masterdata": "masterCountry,masterEthnicity,maritalStatus,masterPreferredLanguage,masterRace,masterState,PatientStatus,Suffix,Relationship,masterprovider,masterProgram,masterService,phonetype,masterInsuranceCompany,masterReferral,insurancePlanType,mastericd" };
  }
  var fieldObj = { "masterdata": "masterCountry,masterEthnicity,maritalStatus,masterPreferredLanguage,masterRace,masterState,PatientStatus,Suffix,Relationship,masterprovider,masterProgram,masterService,phonetype,masterInsuranceCompany,masterReferral,insurancePlanType,mastericd,socialhistory,travelhistory" };

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.master_list.REQUEST));
    fetch(`${GET_MASTER_TABLES_DATA}`, {
      method: "post",
      headers: fetchApiHeaders,
      body: JSON.stringify(fieldObj),
    })
      .then(function(response) {

        if (response.status >= 400) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(function(response) {
        dispatch(getSuccess(patientActionTypes.master_list.SUCCESS, response));
      })
      .catch(function(error) {
      // errorHandler(error);
      });

  };


};

export const checkPatientExist = (formData) => {
  console.log("inside checkPatientExist: ", formData);

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.patient_exist.REQUEST));

    fetch(`${CHECK_PATIENT_EXIST}`, {
      method: "post",
      headers: fetchApiHeaders,
      body: JSON.stringify(formData),
    })
      .then(function(response) {
        console.log("status: ", response.status);
        if (response.status >= 400) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(function(response) {
        let patient_exist = 1;
        if (Object.keys(response.data).length > 0) {
          patient_exist = 2;
        }

        dispatch(getSuccess(patientActionTypes.patient_exist.SUCCESS, patient_exist));
      })
      .catch(function(error) {
        errorHandler(error);
      });

  };

};

export const insertUpdatePatientData = (formData) => {
  // CREATE_PATIENT_API
  console.log("inside insertPatientData: ");

  return (dispatch) => {
    let fetchData = createFetchObj(formData.patient_data, `${CREATE_PATIENT_API}`);
    dispatch(getSuccess(patientActionTypes.create_patient.REQUEST));

    console.log(JSON.stringify(fetchData.formData));

    fetch(`${fetchData.api_url}`, {
      method: fetchData.method,
      headers: fetchApiHeaders,
      body: JSON.stringify(fetchData.formData),
    })
      .then(checkHttpStatus)
      .then(function(response) {
        setLocalStorage("patientID_fk", response.id);

        if (formData.preferenceData != null) {
          formData.preferenceData.patientID_fk = response.id;
          dispatch(insertUpdatePrefrrences(formData.preferenceData));
        }

        let successMessage = "Patient updated successfully.";
        if (formData.patient_data.id == undefined || formData.patient_data.id == null) {
          successMessage = "Patient created successfully.";
        }
        dispatch(getPatientAllInfo(response.id));
        setTimeout(function() {
          Alert.success(`${successMessage}`, {
            html: true,
          });
          dispatch(getSuccess(patientActionTypes.create_patient.SUCCESS, response));
        }, 1000);


      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        errorHandler(alertText);
      });

  };
};

export const mailingAddressAsPrimary = (data) => {
  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.update_mailing_address.SUCCESS, data));
  };
};

export const saveUpdatePatientAddress = (formData) => {
  console.log("inside savePatientAddress: ", JSON.stringify(formData));

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.save_address.REQUEST));
    let fetchData = createFetchObj(formData, `${UPDATE_PATIENT_ADDRESS}`);

    fetch(`${fetchData.api_url}`, {
      method: fetchData.method,
      headers: fetchApiHeaders,
      body: JSON.stringify(formData),
    })
      .then(checkHttpStatus)
      .then(function(response) {
        setLocalStorage("address_id", response.id);

        Alert.success("Address updated successfully.", {
          html: true,
        });
        if (formData.phoneData != null) {
          dispatch(insertPhoneNumbers(formData.phoneData));
        }

        setTimeout(function() {
          dispatch(getPatientAllInfo(formData.patientID_fk));
        }, 1000);


        dispatch(getSuccess(patientActionTypes.save_address.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        errorHandler(alertText);
      });
  };
};

export const insertUpdatePatientInsurance = (insuranceData) => {
  console.log("inside insertUpdatePatientInsurance : ");

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.save_insurance.REQUEST));

    let fetchData = createFetchObj(insuranceData, `${UPDATE_PATIENT_INSURANCE_DETAILS}`);

    console.log(JSON.stringify(fetchData.formData));

    fetch(`${fetchData.api_url}`, {
      method: fetchData.method,
      headers: fetchApiHeaders,
      body: JSON.stringify(fetchData.formData),
    })
      .then(checkHttpStatus)
      .then(function(response) {
        setLocalStorage("insurance_id", response.id);
        Alert.success("Insurance updated successfully.", {
          html: true,
        });
        dispatch(getSuccess(patientActionTypes.save_insurance.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        errorHandler(alertText);
      });
  };
};

export const getPatientData = (patient_id) => {
  console.log("inside getPatientData : ", patient_id);


  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.fetch_patient.REQUEST));
    fetch(`${FETCH_PATIENT}/${patient_id}/?include=patientaddress,patientinsurancedetails,phonenumbers,patientpreference,provider`, {
      method: "get",
      headers: jsonApiHeader,
    })
      .then(checkHttpStatus)
      .then(function(response) {
        const patientObj = response.data.attributes;
        if (response.data.relationships.patientpreference.data[0] != null) {
          let prefrence_data = extractDataFromObject("patient-preference", response.included);
          const patientObj = mergeObjects(response.data.attributes, prefrence_data[0]);
          patientObj.preference_id = response.data.relationships.patientpreference.data[0].id;
        }
        dispatch(getSuccess(patientActionTypes.fetch_patient.SUCCESS, patientObj));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        // errorHandler(alertText);
      });
  };
};

export const insertPhoneNumbers = (formData) => {
  console.log("inside insertPhoneNumbers: ", JSON.stringify(formData));

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.save_phone_number.REQUEST));

    fetch(`${SAVE_PHONE_NUMBER}`, {
      method: "post",
      headers: fetchApiHeaders,
      body: JSON.stringify(formData),
    })
      .then(checkHttpStatus)
      .then(function(response) {
        dispatch(getSuccess(patientActionTypes.save_phone_number.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        errorHandler(alertText);
      });
  };
};

export const getAddressData = (address_id, patient_id) => {
  console.log("inside getAddressData : ", address_id);


  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.fetch_address.REQUEST));

    fetch(`${FETCH_PATIENT_ADDRESS}/${address_id}`, {
      method: "get",
      headers: fetchApiHeaders,
    })
      .then(checkHttpStatus)
      .then(function(response) {
        dispatch(getPatientAllInfo(patient_id));
        dispatch(getSuccess(patientActionTypes.fetch_address.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        // errorHandler(alertText);
      });
  };
};

export const insertUpdatePrefrrences = (formData) => {
  console.log("inside insertUpdatePrefrrences: ", JSON.stringify(formData), formData.id);

  return (dispatch) => {

    let api_endpoint = `${SAVE_PREFRENCES}`;
    if (formData.id != null) {
      api_endpoint = `${UPDATE_PREFERENCES}`;
    }

    let fetchData = createFetchObj(formData, `${api_endpoint}`);

    fetch(`${fetchData.api_url}`, {
      method: fetchData.method,
      headers: fetchApiHeaders,
      body: JSON.stringify(fetchData.formData),
    })
      .then(checkHttpStatus)
      .then(function(response) {
        setLocalStorage("preference_id", response.id);
        dispatch(getSuccess(patientActionTypes.save_preferences.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        errorHandler(alertText);
      });
  };
};

export const getInsuranceData = (insurance_id) => {
  console.log("inside getInsuranceData : ", insurance_id);


  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.fetch_patient_insurance.REQUEST));

    fetch(`${FETCH_PATIENT_INSURANCE}/${insurance_id}`, {
      method: "get",
      headers: fetchApiHeaders,
    })
      .then(checkHttpStatus)
      .then(function(response) {
        dispatch(getSuccess(patientActionTypes.fetch_patient_insurance.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        // errorHandler(alertText);
      });
  };
};

export const getPatientAllInfo = (patient_id) => {
  console.log("inside getPatientAllInfo: ", patient_id);
  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.fetch_patient_all_info.REQUEST));
    fetch(`${FETCH_PATIENT_ALL_INFO}/${patient_id}/?include=patientaddress,patientinsurancedetails,phonenumbers,patientpreference,provider`, {
      method: "get",
      headers: { "Content-Type": "application/vnd.api+json" },
    })
      .then(checkHttpStatus)
      .then(function(response) {
        let patientGlobalData = {
          address_id: response.data.relationships.patientaddress.data[0] ? response.data.relationships.patientaddress.data[0].id : null,
          insurance_id: response.data.relationships.patientinsurancedetails.data[0] ? response.data.relationships.patientinsurancedetails.data[0].id : null,
          phone_numbers: extractDataFromObject("phone-numbers", response.included),
          primary_address: extractDataFromObject("patient-address", response.included),
          patient_profile: response.data.attributes,
        };
        dispatch(getSuccess(patientActionTypes.fetch_patient_all_info.SUCCESS, patientGlobalData));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        errorHandler(alertText);
      });
  };
};

export const getAllPatients = (searchObj) => {
  console.log("inside getAllPatients: ", searchObj);
  return (dispatch) => {

    dispatch(getSuccess(patientActionTypes.fetch_all_patients.REQUEST));

    let query_params = createSearchUrl(searchObj);

    fetch(`${FETCH_ALL_PATIENTS}${query_params}`, {
      method: "get",
      headers: { "Content-Type": "application/vnd.api+json" },
    })
      .then(checkHttpStatus)
      .then(function(response) {

        dispatch(getSuccess(patientActionTypes.fetch_all_patients.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        // errorHandler(alertText);
      });
  };
};

export const clearForm = () => {
  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.clear_form.REQUEST));
  };

};
/* *******************Start patient vitals here******************* */
// Action to get details of all patient vitals
// Poonam
//  18 Jul, 17
export const getPatientVitals = (searchObj) => {
  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.patient_vitals.REQUEST));

    let query_params = createSearchUrl(searchObj);

    fetch(`${FETCH_PATIENT_VITALS}${query_params}`, {
      method: "get",
      headers: jsonApiHeader,
    })
      .then(checkHttpStatus)
      .then(function(response) {
        console.log("response in patient vitals", response);
        dispatch(getSuccess(patientActionTypes.patient_vitals.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        // errorHandler(alertText);
      });
  };
};
/* *******************End patient vitals here******************* */

// Action to get list of state by country id
// Puneet
//  19 Jul, 17
export const getStateByCountry = (country_id) => {
  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.get_state_by_country.REQUEST));
    fetch(`${GET_STATE_BY_COUNTRY}?countryID=${country_id}`, {
      method: "get",
    })
      .then(checkHttpStatus)
      .then(function(response) {
        console.log("response in patient vitals", response);
        dispatch(getSuccess(patientActionTypes.get_state_by_country.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        // errorHandler(alertText);
      });
  };
};

export const getMailingStateByCountry = (country_id) => {
  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.get_state_by_country.REQUEST));
    fetch(`${GET_STATE_BY_COUNTRY}?countryID=${country_id}`, {
      method: "get",
    })
      .then(checkHttpStatus)
      .then(function(response) {
        console.log("response in patient vitals", response);
        dispatch(getSuccess(patientActionTypes.get_mailing_state_by_country.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
        // errorHandler(alertText);
      });
  };
};


// Action to save family history
// Puneet
//  20 Jul, 17
export const saveFamilyHistory = (postData) => {
  console.log("inside saveFamilyHistory: ");

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.save_family_history.REQUEST));

    let api_endpoint = `${FAMILY_HISTORY}`;
    if (postData.id != null) {
      api_endpoint = `${UPDATE_FAMILY_HISTORY}`;
    }

    let fetchData = createFetchObj(postData, `${api_endpoint}`);
    console.log("API send data", JSON.stringify(fetchData.formData));

    fetch(`${fetchData.api_url}`, {
      method: fetchData.method,
      headers: fetchApiHeaders,
      body: JSON.stringify(fetchData.formData),
    })
      .then(checkHttpStatus)
      .then(function(response) {
        let searchObj = {
          filters: { patientid_fk: postData.PatientID_fk },
          includes: "include=masterrelationship,mastergender,mastericd",
        };
        dispatch(clearForm());
        dispatch(getPatientMedicalHistoryList(searchObj));
        Alert.success("Family history added.", {
          html: true,
        });
        dispatch(getSuccess(patientActionTypes.save_family_history.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
      });
  };
};

// Action to fetch patient relations
// Puneet
//  20 Jul, 17
export const getPatientMedicalHistoryList = (searchObj) => {
  console.log("inside getPatientDetailByField: ", searchObj);

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.fetch_patient_relationships.REQUEST));

    let query_params = createSearchUrl(searchObj);

    fetch(`${FAMILY_HISTORY}${query_params}`, {
      method: "get",
      headers: jsonApiHeader,
    })
      .then(checkHttpStatus)
      .then(function(response) {
        dispatch(getSuccess(patientActionTypes.fetch_patient_relationships.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
      });
  };
};

export const deleteFamilyHistory = (history_id, patient_id) => {
  console.log("inside deleteFamilyHistory: ", history_id);

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.save_family_history.REQUEST));
    fetch(`${FAMILY_HISTORY}/${history_id}`, {
      method: "delete",
    })
      .then(checkHttpStatus)
      .then(function() {
        console.log("dddddd");
        let searchObj = {
          filters: { patientid_fk: patient_id },
          includes: "include=masterrelationship,mastergender,mastericd",
        };

        setTimeout(function() {
          Alert.success("Family history Deleted.", {
            html: true,
          });
          dispatch(getPatientMedicalHistoryList(searchObj));
        }, 1000);

        console.log("dddddd===>>>");
        dispatch(getSuccess(patientActionTypes.save_family_history.SUCCESS));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
      });
  };
};

export const fetchHistoryById = (history_id) => {
  console.log("inside fetchHistoryById: ", history_id);

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.fetch_history_detail.REQUEST));
    fetch(`${FAMILY_HISTORY}/${history_id}`, {
      method: "get",
      headers: jsonApiHeader,
    })
      .then(checkHttpStatus)
      .then(function(response) {
        response.data.attributes.DOB = apiInputFormatDate(response.data.attributes.DOB);
        response.data.attributes.DateOfDeath = apiInputFormatDate(response.data.attributes.DateOfDeath);
        dispatch(getSuccess(patientActionTypes.fetch_history_detail.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
      });
  };
};

export const saveSocialHistory = (postData) => {
  console.log("inside saveSocialHistory: ", postData);

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.save_social_history.REQUEST));

    let api_endpoint = `${SOCIAL_HISTORY}`;
    if (postData.id != null) {
      api_endpoint = `${UPDATE_FAMILY_HISTORY}`;
    }

    let fetchData = createFetchObj(postData, `${api_endpoint}`);
    console.log("API send data", JSON.stringify(fetchData));

    fetch(`${fetchData.api_url}`, {
      method: fetchData.method,
      headers: fetchApiHeaders,
      body: JSON.stringify(fetchData.formData),
    })
      .then(checkHttpStatus)
      .then(function(response) {
        dispatch(clearForm());
        Alert.success("Family Social history added.", {
          html: true,
        });
        dispatch(getSuccess(patientActionTypes.save_social_history.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
      });
  };
};

export const getPatientSocialHistory = (searchObj) => {
  console.log("inside getPatientSocialHistory: ", searchObj);

  return (dispatch) => {
    dispatch(getSuccess(patientActionTypes.fetch_social_history.REQUEST));

    let query_params = createSearchUrl(searchObj);

    fetch(`${SOCIAL_HISTORY}${query_params}`, {
      method: "get",
      headers: jsonApiHeader,
    })
      .then(checkHttpStatus)
      .then(function(response) {
        dispatch(getSuccess(patientActionTypes.fetch_social_history.SUCCESS, response));
      })
      .catch(function(error) {
        let alertText = Object.getOwnPropertyDescriptor(error, "message").value;
      });
  };
};

import React, { Component } from 'react'
import DuckImage from './assets/Duck.jpg'
import classes from './Home.scss'
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';


class Home extends Component {
  render() {
    return (
      <div>
        <h4>Welcome! Login Page</h4>
        <FormGroup>
          <Label for="exampleSelect">Select</Label>
          <Input type="select" name="select" id="exampleSelect">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </Input>
        </FormGroup>
      </div>
    )
  }
}

export default Home

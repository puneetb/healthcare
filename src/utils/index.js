import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import moment from 'moment'
import { DEFAULT_PAGE_SIZE } from 'app/globalConstants';

export function createConstants(...constants) {
    return constants.reduce((acc, constant) => {
        acc[constant] = constant;
        return acc;
    }, {});
}

export function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type];

        return reducer
            ? reducer(state, action.payload)
            : state;
    };
}

export function checkHttpStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response
    } else {
        var error = new Error(response.statusText)
        error.response = response
        throw error
    }
}

export function handleRedirect(){
    localStorage.removeItem('authToken');
    browserHistory.push('/login');
}

export function parseJSON(response) {
      return response.data;
}


export function calculateAge(fromdate){
    let todate= new Date();

    var age= [], 
    fromdate= new Date(fromdate),
    y= [todate.getFullYear(), fromdate.getFullYear()],
    ydiff= y[0]-y[1],
    m= [todate.getMonth(), fromdate.getMonth()],
    mdiff= m[0]-m[1],
    d= [todate.getDate(), fromdate.getDate()],
    ddiff= d[0]-d[1];

    if(mdiff < 0 || (mdiff=== 0 && ddiff<0))--ydiff;
    if(mdiff<0) mdiff+= 12;
    if(ddiff<0){
        fromdate.setMonth(m[1]+1, 0);
        ddiff= fromdate.getDate()-d[1]+d[0];
        --mdiff;
    }
    if(ydiff> 0) {
        age.push(ydiff+ ' year'+(ydiff> 1? 's ':' '));
        //if(mdiff> 0) age.push(mdiff+ ' month'+(mdiff> 1? 's ':' '));
    } else if(mdiff> 0) {
        age.push(mdiff+ ' month'+(mdiff> 1? 's ':' '));
        //if(ddiff> 0) age.push(ddiff+ ' day'+(ddiff> 1? 's ':' '));
    } else if(ddiff> 0) {
        age.push(ddiff+ ' day'+(ddiff> 1? 's ':''));
    }
    //f(ydiff> 0) age.push(ydiff+ ' year'+(ydiff> 1? 's ':' '));
    // if(mdiff> 0) age.push(mdiff+ ' month'+(mdiff> 1? 's':''));
    // if(ddiff> 0) age.push(ddiff+ ' day'+(ddiff> 1? 's':''));
    // if(age.length>1) age.splice(age.length-1,0,' and ');    
    return age.join('');
}

export const genderData = {1: 'Male', 2: 'Female', 3: 'Other'}

export function formatDate(date) {
    return moment(date).format('MMM D, YYYY');
}

export function apiInputFormatDate(date) {
    return moment(date).format('YYYY-MM-DD');
}

export function searchFromObj(nameKey, value, myArray) {
    if(myArray != undefined ) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].id == value) {
                return myArray[i].value
            }
        }
    }
}

export function extractDataFromObject(type, myArray) {
    let returnArr = [];
    if(myArray != undefined ) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].type == type) {
                returnArr.push(myArray[i].attributes);
            }
        }
    }
    return returnArr
}


export function setLocalStorage(key, value) {
  localStorage.setItem(key,  value);
}


export function handleFormErrors(errors) {
    /*
     * Filters error list to only those in current window.
     * Alternative of scrollIntoView used to scroll to input field that failed validation
     * 
     * Note: Since `window.scrollTo` does not work unable to implement scrollTo feature
     * Reference: https://github.com/erikras/redux-form/issues/488
     */
    if(errors != null ) {
        let key = Object.keys(errors).reduce((k,l) => {
            return (document.getElementsByName(k)[0].offsetTop < document.getElementsByName(l)[0].offsetTop) ? k : l;
        });
        window.scrollTo(0, document.getElementsByName(key)[0].offsetTop + 200);
    }
    
}

export function createSearchUrl(urlObj) {
    
    //?page[number]=&filter[id]=173&filter[FirstName]=Princey&include=provider,mastergender,patientinsurancedetails&sort=-id,ssn

    let query_params = `?page[number]=${urlObj.page_number ? urlObj.page_number : 1}`
    if(urlObj.sort_fields != null) {
        query_params += `&sort=${urlObj.sort_fields}`
    }

    if(urlObj.filters != null) {
        let filters = urlObj.filters
        for (const key of Object.keys(filters)) {
            query_params += `&filter[${key}]=${filters[key]}`
        }
    }
    
    if(urlObj.includes != null) {
        query_params += `&${urlObj.includes}`
    }    

    
    query_params += `&page[size]=${urlObj.page_size ? urlObj.page_size : DEFAULT_PAGE_SIZE}`
    

    return query_params
}

export function toCamel(o) {
  var newO, origKey, newKey, value
  if (o instanceof Array) {
    newO = []
    for (origKey in o) {
      value = o[origKey]
      if (typeof value === "object") {
        value = toCamel(value)
      }
      newO.push(value)
    }
  } else {
    newO = {}
    for (origKey in o) {
      if (o.hasOwnProperty(origKey)) {
        newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString()
        value = o[origKey]
        if (value !== null && value.constructor === Object) {
          value = toCamel(value)
        }
        newO[newKey] = value
      }
    }
  }
  return newO
}

export function mergeObjects(obj, src) {
    Object.keys(src).forEach(function(key) { obj[key] = src[key]; });
    return obj;
}
```bash
$ git clone https://puneetb@bitbucket.org/puneetb/new-react-starter.git
$ cd new-react-starter
$ npm install                   # Install project dependencies
$ npm start                     # Compile and launch
```

If everything works, you should see the following:
